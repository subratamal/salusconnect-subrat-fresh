"use strict";

define([
	"app",
    "momentWrapper",
    "bluebird",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice",
	"common/model/ayla/mixin.AylaSchedule"
], function (App, moment, P, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B, Mn, $, _) {

		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.MiniSmartPlugUS = B.Model.extend({
			tileType: "smartPlug",
			modelType: constants.modelTypes.MINISMARTPLUG,
			defaults: {
				// get properties
				MACAddress: null, // string, IEEE MAC Address
				SubDeviceName_c: null, // string, Sub device name _c naming is as defined on device wiki
				DeviceType: null, // integer, 16 mini smart plug us
				ManufactureName: null, // string, Manufacture Name
				ModelIdentifier: null, // string, Model ID
				FirmwareVersion: null, // string, Firmware Version
				OnOff: null, // boolean, On Off status
				MainsVoltage_x10: null, // integer, Measured voltage
				SetMeasuredVoltage_x10: null, // integer, Measured voltage
				BatteryVoltage_x10: null, // integer, Battery voltage
				ErrorPowerSLowBattery: null, // boolean, Battery low voltage error
				SummationDeliveredL_x10k: null, // integer, Summation of Energy Lower value
				SummationDeliveredH_x10k: null, // integer, Summation of Energy Higher value
				DemandDelivered_x10k: null, // integer, Current Demond
				Multiplier: null, // integer, Multiplier for actual cumulative energy
				Divisor: null, // integer, Divider for actual cumulative energy
				ErrorMeterSLeakDetect: null, // boolean, Leak Detect error
				ResetSummationDelivered: null, // boolean, Reset Summation Delivered to zero
				DefaultUploadedPeriod: null, // integer, Default report period
				FastPollUpdatePeriod: null, // integer, Fast report period
				FastPollEndTime: null, // integer, Fast Poll end time
				ACPhase_c: null, // integer, save to cloud

				// set properties
				SetOnOff: null,
				SetIndicator: null, //integer, set device indicator
				SetResetSummationDelivered: null, // bool, reset summation delivered to zero
				SetRequestFastPollPeriod: null, // integer (seconds), request fast poll mode period
				SetFastPollModeDuration: null // integer (minutes), request fast poll mode duration
			},

			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
                _.bindAll(this, "_parseDemandDate", "getSummationGraphData");
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 16,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_smart_plug_on_white.svg"),
					device_category_type: constants.categoryTypes.SMARTPLUGS,
					device_category_name_key: "equipment.myEquipment.categories.types.smartPlugs"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_smart_plug_off_grey.svg")
				});
			},
			/**
			 * change the OnOff state of a smartPlug
			 */
			toggleSmartPlug: function () {
				var modelProp = this.get("OnOff"),
					newVal = modelProp.getProperty() ? 0 : 1;

				return modelProp.setProperty(newVal);
			},
			getBaseScheduleProps: function (scheduleId) {
				return [{
					name: this.getSetterPropNameIfPossible("OnOff"),
					base_type: "boolean",
					schedule_id: scheduleId
				}];
			},
             getSummationGraphData: function (timeSpan, startDate, endDate, dateObj) {
               // var that = this, baseProp = "kWhUsageL_ep";
               
				//var that = this, baseProp =  "ep_", basePropApprex = ":sMeterS:avgPower_kW";
                //Bug_SCS-3285
                var that = this, baseProp =  "ep_", basePropApprex = ":sMeterS:sumEnergy_kWh";
                
				var promiseArray = this.get("SummationDeliveredL_x10k").map(function (prop) {

					var index = prop.get("name").split(":")[0].split("_")[1];
					// Fixes bug where seconds was > 60
                    startDate.setHours(0);
                    endDate.setHours(23);

					return App.salusConnector._salusWSConnector.propertyAggregator(that, baseProp+index+basePropApprex, startDate, endDate, timeSpan).then(function (data) {
						prop.mostRecentData = data;
					});
				});
               this._timeSpan= timeSpan;
               this._startDate = startDate;
               this._endDate = endDate;
               this._graphRequests = P.all(promiseArray).then(function () {
                        // injecting the date obj into the promise queue
                        return dateObj;
                    }).then(this._parseDemandDate);
               return this._graphRequests;
                 

				
			},

			killGraphRequests: function () {
				if (this._graphRequests) {
					this._graphRequests.cancel();
				}
			},

			// fills date obj with zeros so all dates have same amount of datapoints
			_fillDateObj: function (dateObj) {
                
				_.each(Object.keys(dateObj), function (key) {
					var data = dateObj[key];

					for(var i = 1; i <= 4; i++) {
						var obj = _.findWhere(data, {dev: "Device" + i});

						if (!obj) {
							data.push({
								dev: "Device" + i,
								value: 0
							});
						}
					}
				});
                
			},

			_parseDemandDate: function (dateObj) {
                var that = this;
                //filter 1h data
                var filterDate = null;
                if(that._timeSpan === "1h"){
                	filterDate = Object.keys(dateObj)[0];
                }
				this.get("SummationDeliveredL_x10k").each(function (prop) {
					var dev = "Device1";
					_.each(prop.mostRecentData, function (datapoint) {
						var date = datapoint.time.split("T")[0];
                        var pointValue = parseFloat(datapoint.value);
                        pointValue = pointValue < 0 ? 0:pointValue;
                        if(that._timeSpan === "1h"){
                            if(date === filterDate){
                                date = datapoint.time.split("T")[1].substr(0,2);
                                date = parseInt(date) + "";
                                if(date.length > 2){
                                    dateObj[date].pop();
                                }
                                
                                if (!dateObj[date]) {
                                    dateObj[date] = [];
                                }
                                
                                //For 1h,do not need to calculate

                                dateObj[date].push({
                                    dev: dev,
                                    value: pointValue
                                });
                            }
                           
                        }else{
                            // TODO: this was a temporary fix for SCS-1668
                            // TODO: figure out why the dateObj doesn't have the date already in place for new data. SCS-1683
                            if(that.isValidDateRange(date)){
                                 if (!dateObj[date]) {
                                    dateObj[date] = [];
                                }

                                //For 1d,value*1, For 1w,value*7, For 1mon,value*current month day
//                                var calCount = 1;
//                                if(that._timeSpan === "1d"){
//                                    calCount = 1;
//                                }else if(that._timeSpan === "1w"){
//                                    calCount = 7;
//                                }else if(that._timeSpan === "1mon"){
//                                    calCount = moment(date).daysInMonth();
//                                }
//
//
//
//                                dateObj[date].push({
//                                    dev: dev,
//                                    value: parseFloat(pointValue) * calCount * 24
//                                });
                                 //Bug_SCS-3285 start
                                dateObj[date].push({
                                    dev: dev,
                                    value: pointValue
                                });
                                //Bug_SCS-3285 end
                            }
                            
                           
                        }

						
                        
					});
				});

				this._fillDateObj(dateObj);

				var returnArray = [];

                var ordered = {};
                Object.keys(dateObj).sort().forEach(function(key) {
                    ordered[key] = dateObj[key];
                });
                
				_.each(Object.keys(ordered), function (key) {
					var data = ordered[key];
					var returnObj = {};
                    returnObj.date = key;
					//returnObj.date = moment(new Date(key)).format("MMM-D");
                    if(that._timeSpan === "1h"){
                        if(key.length > 2){
                            return;
                        }
                        //Dev_SCS-3281 start
                        returnObj.date = key + "";
                        //Dev_SCS-3281 start
                       // returnObj.date = (parseInt(key)+1)+"";
                    }
					_.each(data, function (datapoint) {
						returnObj[datapoint.dev] = datapoint.value || 0;
					});
                    
                    

					returnArray.push(returnObj);
				});

				return returnArray;
			},
             isValidDateRange: function(singleDate){
                var that = this;
                var startDate = moment(that._startDate).format("YYYY-MM-DD");
                var endDate = moment(that._endDate).format("YYYY-MM-DD");
                var isBetween = moment(singleDate).isBetween(startDate, endDate); // true
                var isSame1 = singleDate === startDate; 
                var isSame2 = singleDate === endDate; 
                if(isBetween || isSame1 || isSame2){
                   return true;
                }   
                return false;
            },
			_getWritePropIfExists: function (writeProps, key, nameIdentifier) {
					var contains = false, writeProp = null;

					contains = _.reduce(writeProps, function (contains, thisWriteProp) {
						var returnVal;
						if (contains) {
							returnVal = contains;
						} else if (thisWriteProp.display_name === key && thisWriteProp.name.indexOf(nameIdentifier) === 0) {
							returnVal = true;
							writeProp = thisWriteProp;
						} else {
							returnVal = false;
						}

						return returnVal;
					}, contains);

					if (contains) {
						return writeProp;
					} else {
						return false;
					}
			},
			// much of this functionality is duplicated from _parseDeviceProperty in mixin.BaseDevice,
			// please be careful before touching this.
			_parseDeviceProperty: function (prop, writeProps) {
				var linkedWriteProp,
						key = prop.display_name, setKey = "Set" + key,
						identifier = prop.name.split(":")[0];

				if (writeProps && writeProps.length > 0) {
					linkedWriteProp = this._getWritePropIfExists(writeProps, setKey, identifier);

					if (linkedWriteProp) {
						this.updateOrCreateProp(key, [null, {
							getterProp: prop,
							setterProp: linkedWriteProp
						}], Models.LinkedDevicePropertyModel, identifier);
					} else {
						this.updateOrCreateProp(key, [prop], Models.DevicePropertyModel, identifier);
					}
				} else {
					this.updateOrCreateProp(key, [prop], Models.DevicePropertyModel, identifier);
				}
                
				if (!this.get(key).hasListenToChangeValue) {
                    var that = this;
					this.listenTo(this.get(key), "change:value", function () {
						that.trigger("change:" + key, that.get(key));
					});
					this.get(key).hasListenToChangeValue = true;
				}

				return writeProps || true;
			},

			updateOrCreateProp: function (key, args, Class, identifier) {
				if (this.get(key)) {
					this.get(key).addOrUpdate(key, args, Class, identifier);
				} else {
					this.set(key, new Models.MulitplePropertyCollection());
					this.get(key).addOrUpdate(key, args, Class, identifier);
					this.trigger("change:" + key, this.get(key));
				}
			}
		}).mixin([BaseAylaDevice, Models.AylaScheduleMixin]);
	});

	return App.Models.MiniSmartPlugUS;
});