"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/equipment/views/GroupPage.view",
    "consumer/dashboard/views/Tile.view",
    "consumer/dashboard/views/tileContentViews/AddGatewayTile.view",
    "jquery-ui"
], function(App, P, constants, consumerTemplates) {
    App.module("Consumer.Dashboard.Views", function(Views, App, B, Mn, $, _) {

        Views.TileManagerView = Mn.CompositeView.extend({

            className: "bb-tile-manager tile-manager disable-text-select col-xs-12 margin-b-30",

            template: consumerTemplates["dashboard/tileManager"],

            ui: {
                tileContainer: ".bb-dashboard-tile-manager-container",
                groupContainer: ".bb-dashboard-group-container"
            },

            events: {
                //touchmove: "_handleDragEvent"
                "tap .bb-dashboard-group-container__close": "closeModal"
            },

            childView: Views.TileView,

            childViewContainer: ".bb-dashboard-tile-manager-container",

            initialize: function(options) {
                var that = this;

                options = options || {};

                _.bindAll(this,
                    "_lockTiles",
                    "_unlockTiles",
                    "_setTileLockOnFlip",
                    "_handleTileFlip",
                    "_dismissWelcomeTile",
                    "_updateTileOrder",
                    "_doOnOrientationChange"
                );

                // this._handleDragEvent = _.debounce(this._handleDragEvent, 16);
                this.type = options.type || "large";
                this.dashboardManager = options.dashboardManager;

                if (options.shouldBeHidden) {
                    this.$el.addClass("not-visible");
                }

                if(window.cordova){
                    window.addEventListener('orientationchange', this._doOnOrientationChange);
                }

                this.$el.attr("data-gateway-dsn", options.gatewayDSN);

                this.tiles = [];
                this.$el.addClass("tile-manager-type-" + this.type);

                // Add the tile drag backdrop to the dom
                this.$tileDragBackdrop = $(_.template("<div class='bb-tile-drag-backdrop dashboard-backdrop fade'></div>")());
                $('body').append(this.$tileDragBackdrop);

                this.listenTo(this.collection, "reorder:tileOrder", function() {
                    that.collection.trigger("tileOrder:modification", that.collection);
                });

                this.listenTo(this, "childview:show:group", function(view, groupId) {
                  this.showGroup(groupId);
                }, this);

                 this.listenTo(this, "childview:close:group", function(view, groupId) {
                  this.closeModal(groupId);
                }, this);
            },

            onAddChild: function(childView) {
                var that = this;
                this.tiles.push(childView);
                if (childView.model.get("isFlipped")) {
                    childView.model.set("isFlipped", false);
                }

                this.listenTo(childView, "flipComplete", this._handleTileFlip);

                if(!window.cordova) {
                    this.listenTo(childView, "longPress", this._enableDragMode);
                } else {
                    this.listenTo(childView, "longPress", this._decideTileMoverMode);
                    this.listenTo(childView, "tileMover:actionButton:click", this._handleTileMoverNavigation);
                }

                this.listenTo(childView.backView, "unpinned", function() {
                    // relayout packery upon unpinning
                    setTimeout(function() {
                        // that.ui.tileContainer.packery("layout");
                    }, 0);
                });


                // listen for the welcome tile to dismiss
                if (childView.model.get("referenceType") === "welcome") {
                    this.listenTo(childView.frontView, "dismiss", this._dismissWelcomeTile);
                }

            },

            getTileModelsInOrder: function() {
                var that = this,
                    models = [],
                    tiles = this.ui.tileContainer.children();

                _.each(tiles, function(tile) {
                    var cid = $(tile).data("view-cid"),
                        tileView = that.children.findByCid(cid);

                    if (tileView) {
                        models.push(tileView.model.toJSON());
                    }
                });

                return models;
            },

            onRender: function() {
                if (this.collection && this.collection.gatewayDsn === constants.noGateway) {
                    if (!this.addGatewayTile) {
                        this.addGatewayTile = new App.Consumer.Dashboard.Views.TileContentViews.AddGatewayTile();
                    }

                    this.ui.tileContainer.append(this.addGatewayTile.render().$el);
                }
            },

            completeOnShow: function() {
                $(".bb-spinner").remove();
                this.children.each(function(childView) {
                    childView.completeOnShow();
                });

                this._triggerSortable();
                // this._disableDragMode();

                if (this.collection && this.collection.firstLoad) {
                    this._updateTileOrder();
                    this.collection.firstLoad = false;
                }

                this.dashboardManager.get("sessionUser").dashboardFirstLoad = false;
            },
            /**
             * Toggles the tile lock if any of the tiles are currently flipped over
             * @private
             */
            _setTileLockOnFlip: function(flippedView) {
                if (this.anyTileFlipped()) {
                    this._disableDragMode();
                    this._lockTiles();
                } else {
                    // Re-enables sortable after the tile flip completed.
                    this._unlockTiles();
                }
            },
            anyTileFlipped: function() {
                return this.$(".flipped").length > 0;
            },
            _handleTileFlip: function(flippedView) {
                if(this.tileMoverModeActive) {
                    var data = {
                        evt: flippedView.$el,
                        view: this.currentTileMover
                    }
                    this._decideTileMoverMode(data);
                }
                this._setTileLockOnFlip(flippedView);

                this.children.each(function(childView) {
                    if (childView !== flippedView) {
                        childView.closeTile();
                    }
                });
            },


            /**
             * Starts sortable
             * @private
             */
            _triggerSortable: function() {

                var itemElems, that = this,
                    normalTileWidth;

                /**
                 * Since our tiles grow and shrink in size, set the packery column width to the hard coded value of
                 * a normal sized tile (145px). This allows packery to quickly calculate the width of a column of tiles
                 * when they grow / rearrange. This fixes an issue where if a thermostat tile in the first position of the
                 * container was flipped over and grown to a 2x2 size, the rest of the tiles would inherit the spacing
                 * of a 2x2 tile while visibly looking like a 1x1 tile.
                 */
                normalTileWidth = 145; // in px
                this.ui.tileContainer.css('height', parseInt(this.ui.tileContainer.css('height'), 10) + normalTileWidth + 13);

		// Don't implement sortble in cordova env.
                if(!window.cordova) {
                    var cancelRequired = false;
                    this.ui.tileContainer.sortable({
                        containment: this.ui.tileContainer,
                        delay: 1000,
                        scroll: true,
                        disable: false,
                        scrollSensitivity: 100,
                        scrollSpeed: 5,
                        tolerance: "pointer",
                        cancel: ".double-width",
                        zIndex: 2,


                        start: function(event, ui) {
                            that._enableDragMode();
                        },

                        sort: function(event, ui) {
                            if (!that.$tileDragBackdrop.hasClass("in")) {
                                that.$tileDragBackdrop.addClass("in");
                            }
                            that.$el.addClass('drag-mode-enabled');
                            that._handleDragEvent();
                        },
                        beforeStop: function(event, ui) {
                            if(ui.item.index() == 0) {
                                if($(ui.item).parent().children().hasClass("double-width")){
                                    cancelRequired = true;
                                }
                            }
                        },
                        stop: function(event, ui) {
                            that._handleDragEventEnd();
                            that._updateTileOrder();
                            if (cancelRequired) {
                                $(this).sortable('cancel');
                                cancelRequired = false;
                            }
                        }
                    });
                }
            },

            showGroup: function(groupId) {
              //this.ui.groupContainer.height(this.ui.tileContainer.height());
                if (this.anyTileFlipped()) {
                    this._handleTileFlip();
                }
              this.ui.groupContainer.removeAttr("style");
                if (this.groupPageView) {
                    this.groupPageView.destroy();
                }

                App.showGroupId = groupId;
                var group = App.salusConnector.getGroup(groupId);
                var onlyPinnedDevices = group.get("pinned_devices");

                //Update order
                var currentGatewayDSN = App.getCurrentGatewayDSN();
                var collection = App.salusConnector.getSessionUser().getTileOrderForGateway(currentGatewayDSN)

                var collectionDevicesOrder = []
                _.each(collection.models, function(deviceModel) {
                    if(onlyPinnedDevices.indexOf(deviceModel.get('referenceId')) != -1) {
                        collectionDevicesOrder.push(deviceModel.get('referenceId'));
                    }
                });

                var devices = _.map(collectionDevicesOrder, function (deviceId) {
                var device = App.salusConnector.getDevice(deviceId);
                return new B.Model({
                    referenceId: device.get("key"),
                    referenceType: "device",
                    isLargeTile: true,
                    isFlipped: false
                  });
              });

                if (devices.length && devices[0]) {
                    this.groupPageView = new Views.TileManagerView({
                        dashboardManager: this.dashboardManager,
                        collection: new B.Collection(devices),
                        gatewayDSN: this.getOption("gatewayDSN"),
                        shouldBeHidden: false
                    });

                    $(this.ui.groupContainer).css("min-height", this.$el.height() + 30);
                    this.ui.groupContainer.append(this.groupPageView.render().el);
                    this.groupPageView.completeOnShow();
                    this.ui.groupContainer.find(".bb-dashboard-group-container-name").text(group.attributes.name);
                    this.ui.groupContainer.show();
                    var y = $(".bb-dashboard-header").height() - 30;
                    $('html, body').animate({scrollTop: y}, 1000);
                    App.groupPageView = this;
                }
            },

            /* On Orientation change In order to fix the tiles movie out of scope we close tiles */
            _doOnOrientationChange: function() {
                this.closeModal();
                if (this.anyTileFlipped()) {
                    this._handleTileFlip();
                }
            },

            closeModal: function(flipTilePromise) {
                var that = this;
                if (!(flipTilePromise instanceof P.Promise)) {
                  flipTilePromise = P.resolve([]);
                }

                return new P.Promise(function(resolve, reject) {
                    if(window.cordova && App.currentTileMover && App.tileMoverModeActive){
                        var data = {
                            evt: App.currentTileMover.$el,
                            view: App.currentTileMover
                        };
                        App.tileManagerView._decideTileMoverMode(data);
                    }

                    if (that.groupPageView) {
                        _.each(that.tiles, function(tile){
                             if((tile.model.get("referenceType") == "group" && tile.model.get("referenceId") == App.showGroupId)) {
                                var group = App.salusConnector.getGroup(tile.model.get("referenceId"));
                                var deviceIds = group.get("devices");
                                console.info('deviceIds : : ',deviceIds);
                                var pinnedDevicesOnly = _.filter(group.get("devices"), function(deviceId) {
                                        var deviceModel = App.salusConnector.getDevice(deviceId);
                                        if (deviceModel.get("isInDashboard")) {
                                            return deviceId;
                                        }
                                    });

                                if(pinnedDevicesOnly.length) {
                                    tile.frontView.model.set('pinned_device_count', pinnedDevicesOnly.length);
                                    try {
                                        tile.frontView.render();
                                    } catch(e) {

                                    }
                                } else {
                                    try {
                                        tile.destroy();
                                    } catch(e) {
                                    }
                                }
                             }
                        });

                        flipTilePromise
                        .then(function(){
                            that.ui.groupContainer.hide("fast", function() {
                            // that.ui.groupContainer[0].removeChild(that.groupPageView.el);
                                if( that.groupPageView){
                                    if(!window.cordova){
                                        that.groupPageView.ui.tileContainer.sortable("destroy");
                                    }
                                    that.groupPageView.destroy();
                                }
                                App.showGroupId = null;
                                App.groupPageView = null; // set null once group modal is closed on dashboard
                                resolve(true);
                            });
                        })
                    }
                });
            },

            closeTiles:function(){

                _.each(this.children._views,function(childView){
                        childView.closeTile();
                },this);
            },
            /*_handleDragEventStart: function() {
                console.info('Drag Start');
                var $body = $("body");

                // tiles dragged outside body
                $body.addClass("overflow-hidden");

                // This will disable scrolling of the page while the user is interacting with an element inside it
                $body.attr("ontouchmove", "event.preventDefault();").attr("ontouchstart", "event.preventDefault();");
            },*/
            _handleDragEventEnd: function() {

                var $body;

                // This handles the case where the user is dragging multiple tiles around at once, drops one, and
                // continues dragging the another one. This prevents us from "locking" the tiles if a drag is still in progress
                if ($(".ui-sortable-helper").length === 0) {
                    // console.info('Drag End');
                    clearInterval(this.dragCancelTimer);
                    this.dragCancelTimer = null;
                    this._disableDragMode();
                }
            },
            /**
             * Handler for dragging a tile and triggering a scroll even on the screen
             * when the tile is moved to the top or bottom of the screen
             * @private
             */
            _handleDragEvent: function(e) {
                // console.info('Handle Drag');
                // this.ui.tileContainer.sortable("widget")._mouseStart(e);

                // Only trigger assisted screen scroll when a tile is being dragged
                if (this.$el.hasClass("drag-mode-enabled")) {
                    var screenHeight = screen.height,
                        topBound = screenHeight * (1 / 5),
                        bottomBound = screenHeight * (3 / 5),
                        y = $(window).scrollTop(),
                        topOfTilePixelsFromTopOfScreen;
                    if ($(".ui-sortable-helper").length) {

                        topOfTilePixelsFromTopOfScreen = $(".ui-sortable-helper").offset().top - $(window).scrollTop();
                        if (topOfTilePixelsFromTopOfScreen < topBound) {
                            this._animateScrollTop(y - 250, 600);
                        } else if (topOfTilePixelsFromTopOfScreen > bottomBound) {
                            if (y + 15 < screenHeight) {
                                // make sure we don't go below the screen
                                this._animateScrollTop(y + 250, 600);
                            }
                        }
                    }
                } else {
                    // TODO: smoothen the animation.

                    window.ee = e;
                    var deltaY = this.scrollStartPos - e.touches[0].pageY;
                    // var delta = e.touches[0].pageY - this.touchStartY;
                    // this.touchStartY = e.touches[0].pageY;
                    // document.body.scrollTop = document.body.scrollTop + deltaY;

                    var body = $("html, body");
                    body.stop().animate({
                        scrollTop: (document.body.scrollTop + deltaY)
                    }, '200', 'swing', function() {
                       console.log("Finished animating");
                    });

                    // e.preventDefault();
                    // console.info("Scroll please" + delta + " " + this.scrollStartPos + " " + e.touches[0].pageY);
                }
            },

            /**
             * Animate the window scroll during a tile drag. Smooth as butter.
             * @param target
             * @param duration
             * @private
             */
            _animateScrollTop: function(target, duration) {
                // console.info('Animate Scroll');
                duration = duration || 16;

                var that = this,
                    scrollTopProxy = { value: $(window).scrollTop() };

                if (scrollTopProxy.value !== target && !this.isScrolling) {
                    this.isScrolling = true;

                    $(scrollTopProxy).animate({ value: target }, {
                        duration: duration,
                        easing: 'swing',
                        step: function(stepValue) {
                            var rounded = Math.round(stepValue);
                            $(window).scrollTop(rounded);
                        },
                        complete: function() {
                            setTimeout(function() {
                                that.isScrolling = false;
                            }, 400);
                        }
                    });
                }
            },

            _enableDragMode: function(data) {
                var that = this;
                // console.info('Enable Drag');
                // Flip over any tiles that are flipped so we can enable drag move from a flipped tile state
                if (this.anyTileFlipped()) {
                    this._handleTileFlip();
                }
                this.$tileDragBackdrop.addClass("in");
                this.$el.addClass("drag-mode-enabled");
                this._unlockTiles();

                // Setup a drag cancel timer that checks if the user is still dragging and cancels it if they are not
                if (!this.dragCancelTimer) {
                    this.dragCancelTimer = setInterval(function() {
                        // console.info('dragCancelTimer');
                        if (!$(".ui-sortable-helper").length) {
                            that._handleDragEventEnd();
                        }
                    }, 1500);
                }
                /*setTimeout(function() {
                    var offset = $(e.currentTarget).offset();
                    var type = $.mobile.touchEnabled ? 'touchstart' : 'mousedown';
                    var newevent = $.Event(type);
                    newevent.which = 1;
                    newevent.target = $(e.currentTarget);
                    newevent.pageX = e.pageX ? e.pageX : offset.left;
                    newevent.pageY = e.pageY ? e.pageX : offset.top;

                    $(e.currentTarget).trigger(newevent);
                }, 15)
                return false;*/
            },

            _disableDragMode: function() {
                // console.info('Disable Drag');
                var that = this;

                this.$tileDragBackdrop.removeClass("in");
                this.$el.removeClass("drag-mode-enabled");
            },

            _decideTileMoverMode: function(data) {
                var that = this;

                var data = data || {};
                var evt = data.evt, view = data.view;

                var checkIfDoubleWidthTile = view.$el.hasClass("double-width");
                if(checkIfDoubleWidthTile) {
                    return;
                }

                var longPressOnSameTile = false;
                if(that.currentTileMover == view) {
                    longPressOnSameTile = true;
                }

                if(this.tileMoverModeActive) {
                    this._disableTileMoverMode(evt, view);

                    if(longPressOnSameTile) {
                        // Happened on the same tile and thus bring the tile mover to closure.
                        // Persist the tile order at this stage.
                        that._updateTileOrder();
                        return;
                    }
                }

                this._enableTileMoverMode(evt, view);
            },

            _enableTileMoverMode: function(evt, view) {
                this.tileMoverModeActive = App.tileMoverModeActive = true;
                this.currentTileMover = App.currentTileMover = view;
                App.tileManagerView = this;
                // activeTileMover

                // Flip over any tiles that are flipped so we can enable drag move from a flipped tile state
                if (this.anyTileFlipped()) {
                    this._handleTileFlip();
                }

                this.currentTileMover.$el.addClass("tile-mover-mode-active");
                this._unlockTiles();

                this._decorateTileWithIndicators(view);
                this._attachTileMoverEvents(view);
            },

            _disableTileMoverMode: function(evt, view) {
                this.tileMoverModeActive = false;
                this.currentTileMover = null;
                if(App.currentTileMover){
                    App.currentTileMover = null;
                }

                if(App.tileMoverModeActive){
                    App.tileMoverModeActive = false;
                }

                if(App.tileManagerView) {
                    App.tileManagerView = null;
                }

                $(".tile-container", this.$el).removeClass("tile-mover-mode-active");

                this._dettachTileMoverEvents();
                this._removeIndicatorsFromTile();
            },

            _decorateTileWithIndicators: function(view) {
                this.ui.tilesWithSingleGrid = $(".tile-container.bb-tile-container:not('.double-width')", this.$el);
                var checkIfDoubleWidthTile = this.ui.tileContainer.find(".bb-tile-container.double-width").length,
                totalNoOfTiles = this.ui.tilesWithSingleGrid.length,
                tileContainerWidth = this.ui.tileContainer.innerWidth(),
                tileWidth = this.ui.tilesWithSingleGrid.eq(totalNoOfTiles-1).outerWidth(true),
                tilesPerGrid = Math.floor(tileContainerWidth/tileWidth);

                if(checkIfDoubleWidthTile) {
                    this.ui.tilesWithSingleGrid.splice(0, 0, 0, 0);
                }

                var directionConfigs = this._calculateDirectionalArrows(view, {
                    tilesPerGrid: tilesPerGrid
                });

                // TODO: Need refactoring.
                var directionalActionButtons = _.template('<div class="carousel-control-wrapper">   <% if(directionObj.top) { %>    <a class="carousel-control up" href="javascript:void(0);" data-slide="up" data-navigate-element-index="<%=directionObjIndex.top%>">    <span class="glyphicon glyphicon-chevron-up"></span> </a>    <% } %> <% if(directionObj.bottom) { %> <a class="carousel-control bottom" href="javascript:void(0);" data-slide="bottom" data-navigate-element-index="<%=directionObjIndex.bottom%>"><span class="glyphicon glyphicon-chevron-down"></span></a><% } %> <% if(directionObj.left) { %>   <a class="carousel-control left" href="javascript:void(0);" data-slide="left" data-navigate-element-index="<%=directionObjIndex.left%>"><span class="glyphicon glyphicon-chevron-left"></span>  </a>    <% } %> <% if(directionObj.right) { %>  <a class="carousel-control right" href="javascript:void(0);" data-slide="right" data-navigate-element-index="<%=directionObjIndex.right%>"><span class="glyphicon glyphicon-chevron-right"></span>  </a>    <% } %></div>')(directionConfigs);
                //$(".tile-content-view", view.$el).append(directionalActionButtons);
                $(".front", view.$el).append(directionalActionButtons);
            },

            _removeIndicatorsFromTile: function() {
                $(".carousel-control-wrapper").remove();
            },

            _attachTileMoverEvents: function(view) {
                //$(".tile-content-view .carousel-control", view.$el).on("click", view._handleStaticClick);
                $(".carousel-control", view.$el).on("click", view._handleStaticClick);
            },

            _dettachTileMoverEvents: function() {
                //$(".tile-content-view .carousel-control").off("click");
                $(".carousel-control").off("click");
            },

            _calculateDirectionalArrows: function(view, options) {
                // (n-gridSize),(n+1),(n+gridSize),(n-1)
                options = options || {};
                var tilesPerGrid = options.tilesPerGrid;
                var checkIfDoubleWidthTile = this.ui.tileContainer.find(".bb-tile-container.double-width").length;

                // this.ui.tilesWithSingleGrid = $(".tile-container.bb-tile-container:not('.double-width')", this.$el);
                var currentTileIndex = this.ui.tilesWithSingleGrid.index(view.$el);
                var directionObjIndex = {
                    top: currentTileIndex - tilesPerGrid,
                    right: (currentTileIndex + 1)%tilesPerGrid == 0 ? -1 : currentTileIndex + 1,
                    bottom: currentTileIndex + tilesPerGrid,
                    left: currentTileIndex%tilesPerGrid == 0 ? -1 : currentTileIndex - 1
                };

                var directionObj = {
                    top: this.ui.tilesWithSingleGrid[directionObjIndex.top] ? true : false,
                    right: this.ui.tilesWithSingleGrid[directionObjIndex.right] ? true : false,
                    bottom: this.ui.tilesWithSingleGrid[directionObjIndex.bottom] ? true : false,
                    left: this.ui.tilesWithSingleGrid[directionObjIndex.left] ? true : false
                };

                // Adjust two grid position for welcome tile
                if(checkIfDoubleWidthTile) {
                    directionObjIndex = {
                        top: directionObjIndex.top - 2,
                        right: directionObjIndex.right - 2,
                        bottom: directionObjIndex.bottom - 2,
                        left: directionObjIndex.left - 2
                    };
                }

                return {
                    directionObj: directionObj,
                    directionObjIndex: directionObjIndex
                }
            },

            _handleTileMoverNavigation: function(data) {
                data = data || {};
                var evt = data.evt, view = data.view;
                var $target = $(evt.currentTarget);

                var tilesWithSingleGrid = $(".tile-container.bb-tile-container:not('.double-width')", this.$el);

                var navigateElemIndex = $target.attr("data-navigate-element-index");
                var currentTileIndex = tilesWithSingleGrid.index(view.$el);
                var tileToSwap = tilesWithSingleGrid.eq(navigateElemIndex);

                var tilesToSlide = [];
                var upSlideTile = $target.hasClass("up") ? true : false;
                var downSlideTile = $target.hasClass("bottom") ? true : false;

                if(tileToSwap.length) {
                    if(upSlideTile || downSlideTile) {
                        // if(currentTileIndex > navigateElemIndex) {
                        //     for(var idx = currentTileIndex-1, idx>navigateElemIndex-1, idx--) {
                        //         tilesToSlide.push($tiles.eq(idx));
                        //     }
                        // } else {
                        //     for(var idx = currentTileIndex+1, idx<=navigateElemIndex, idx++) {
                        //         tilesToSlide.push($tiles.eq(idx));
                        //     }
                        // }

                        this._slideTiles(view.$el, tileToSwap, {
                            tilesToSlide: tilesToSlide,
                            upSlide: upSlideTile,
                            opacity: "0.5",
                            speed: 500,
                            currentTileindex: currentTileIndex,
                            navigateTileIndex: navigateElemIndex,
                            decorateCallback: this._decorateTileWithIndicators,
                            attachEventCallback: this._attachTileMoverEvents
                        });
                    } else {
                        this._swapTiles(view.$el, tileToSwap, {
                            opacity: "0.5",
                            speed: 500,
                            currentTileindex: currentTileIndex,
                            navigateTileIndex: navigateElemIndex,
                            decorateCallback: this._decorateTileWithIndicators,
                            attachEventCallback: this._attachTileMoverEvents
                        });
                    }

                    this._dettachTileMoverEvents();
                    this._removeIndicatorsFromTile();
                }
            },

            _slideTiles: function(currentTile, destinationTile, options) {
                var that = this, $tileContainer = $(".dashboard-tile-manager-container", this.$el);
                var defaults = {
                    speed: 500,
                    opacity: "1"
                };

                var tilesWithSingleGrid = $(".tile-container.bb-tile-container:not('.double-width')", this.$el);
                var checkIfDoubleWidthTile = this.ui.tileContainer.find(".bb-tile-container.double-width").length;

                function insertTileAtIndex($tile, idx) {
                    if(checkIfDoubleWidthTile) {
                        idx = idx + 1;
                    }

                    if(idx == 0) {
                        //$tileContainer.prepend($tile);
                        tilesWithSingleGrid.eq(0).before($tile);
                        return;
                    }
                    $(".tile-container:nth-child(" + (idx) + ")", $tileContainer).after($tile);
                }

                var options = $.extend(defaults, options);
                var currentTileindex = parseInt(options.currentTileindex), navigateTileIndex = parseInt(options.navigateTileIndex);
                var tilesToSlide = options.tilesToSlide;
                var upSlide = options.upSlide;

                // set primary and secondary elements to relative if not already specified a positon CSS attribute
                var currentPrimaryPos = currentTile.css("position");
                var currentSecondaryPos = destinationTile.css("position");
                if (currentPrimaryPos!="relative" && currentPrimaryPos!="absolute") {
                    currentTile.css("position", "relative");
                }
                if (currentSecondaryPos!="relative" && currentSecondaryPos!="absolute") {
                    destinationTile.css("position", "relative");
                }

                // calculate y-axis movement
                var currentPrimaryPosition = currentTile.offset();
                var currentPrimaryTop = currentPrimaryPosition.top;
                var currentSecondaryPosition = destinationTile.offset();
                var currentSecondaryTop = currentSecondaryPosition.top;
                var directionPrimaryY = '-';
                var directionSecondaryY = '-';
                if (currentPrimaryTop <= currentSecondaryTop) { // if primary above secondary
                    var directionPrimaryY = '+';
                    var totalY = currentSecondaryTop - currentPrimaryTop;
                } else { // if primary below secondary
                    var totalY = currentPrimaryTop - currentSecondaryTop;
                }
                if (directionPrimaryY=='-') {
                    directionSecondaryY='+';
                } else {
                    directionSecondaryY='-';
                }

                // calculate x-axis movement
                var currentPrimaryPosition = currentTile.offset();
                var currentPrimaryLeft = currentPrimaryPosition.left;
                var currentSecondaryPosition = destinationTile.offset();
                var currentSecondaryLeft = currentSecondaryPosition.left;
                var directionPrimaryX = '-';
                var directionSecondaryX = '-';
                if (currentPrimaryLeft <= currentSecondaryLeft) { // if primary left of secondary
                    var directionPrimaryX = '+';
                    var totalX = currentSecondaryLeft-currentPrimaryLeft;
                } else { // if primary below secondary
                    var totalX = currentPrimaryLeft-currentSecondaryLeft;
                }
                if (directionPrimaryX=='-') {
                    directionSecondaryX='+';
                } else {
                    directionSecondaryX='-';
                }

                $.when(
                    currentTile.animate({
                        opacity: options.opacity
                    }, 100),
                    currentTile.animate({
                        top: directionPrimaryY+"="+(totalY)+"px",
                        left: directionPrimaryX+"="+(totalX)+"px"
                    }, options.speed),
                    currentTile.animate({
                        opacity: "1"
                    }, 100)
                ).done(function(){
                    currentTile.removeAttr('style');

                    if(upSlide) {
                        insertTileAtIndex(currentTile, navigateTileIndex);
                    } else {
                        insertTileAtIndex(currentTile, navigateTileIndex+1);
                    }

                    // if(upSlide) {
                    //     tilesToSlide.forEach(function($tile, index){
                    //         insertTileAtIndex($tile, currentTileindex);
                    //         --currentTileindex;
                    //     });
                    // } else {
                    //     tilesToSlide.forEach(function($tile, index){
                    //         insertTileAtIndex($tile, currentTileindex);
                    //         --currentTileindex;
                    //     });
                    // }

                    options.decorateCallback.call(that, that.currentTileMover);
                    options.attachEventCallback.call(that, that.currentTileMover);
                });
            },

            _swapTiles: function(currentTile, tileToSwap, options) {
                var that = this, $tileContainer = $(".dashboard-tile-manager-container", this.$el);
                var defaults = {
                    speed: 500,
                    opacity: "1"
                };

                var tilesWithSingleGrid = $(".tile-container.bb-tile-container:not('.double-width')", this.$el);
                var checkIfDoubleWidthTile = this.ui.tileContainer.find(".bb-tile-container.double-width").length;

                function insertTileAtIndex($tile, idx) {
                    if(checkIfDoubleWidthTile) {
                        idx = idx + 1;
                    }

                    if(idx == 0) {
                        //$tileContainer.prepend($tile);
                        tilesWithSingleGrid.eq(0).before($tile);
                        return;
                    }
                    $(".tile-container:nth-child(" + (idx) + ")", $tileContainer).after($tile);
                }

                var options = $.extend(defaults, options);
                var currentTileindex = options.currentTileindex, navigateTileIndex = parseInt(options.navigateTileIndex);
                if (tileToSwap !== "") {
                    // set primary and secondary elements to relative if not already specified a positon CSS attribute
                    var currentPrimaryPos = currentTile.css("position");
                    var currentSecondaryPos = tileToSwap.css("position");
                    if (currentPrimaryPos!="relative" && currentPrimaryPos!="absolute") {
                        currentTile.css("position", "relative");
                    }
                    if (currentSecondaryPos!="relative" && currentSecondaryPos!="absolute") {
                        tileToSwap.css("position", "relative");
                    }

                    // calculate y-axis movement
                    var currentPrimaryPosition = currentTile.offset();
                    var currentPrimaryTop = currentPrimaryPosition.top;
                    var currentSecondaryPosition = tileToSwap.offset();
                    var currentSecondaryTop = currentSecondaryPosition.top;
                    var directionPrimaryY = '-';
                    var directionSecondaryY = '-';
                    if (currentPrimaryTop <= currentSecondaryTop) { // if primary above secondary
                        var directionPrimaryY = '+';
                        var totalY = currentSecondaryTop - currentPrimaryTop;
                    } else { // if primary below secondary
                        var totalY = currentPrimaryTop - currentSecondaryTop;
                    }
                    if (directionPrimaryY=='-') {
                        directionSecondaryY='+';
                    } else {
                        directionSecondaryY='-';
                    }

                    // calculate x-axis movement
                    var currentPrimaryPosition = currentTile.offset();
                    var currentPrimaryLeft = currentPrimaryPosition.left;
                    var currentSecondaryPosition = tileToSwap.offset();
                    var currentSecondaryLeft = currentSecondaryPosition.left;
                    var directionPrimaryX = '-';
                    var directionSecondaryX = '-';
                    if (currentPrimaryLeft <= currentSecondaryLeft) { // if primary left of secondary
                        var directionPrimaryX = '+';
                        var totalX = currentSecondaryLeft-currentPrimaryLeft;
                    } else { // if primary below secondary
                        var totalX = currentPrimaryLeft-currentSecondaryLeft;
                    }
                    if (directionPrimaryX=='-') {
                        directionSecondaryX='+';
                    } else {
                        directionSecondaryX='-';
                    }

                    $.when(
                        currentTile.animate({
                            opacity: options.opacity
                        }, 100),
                        currentTile.animate({
                            top: directionPrimaryY+"="+(totalY)+"px",
                            left: directionPrimaryX+"="+(totalX)+"px"
                        }, options.speed),
                        currentTile.animate({
                            opacity: "1"
                        }, 100),
                        tileToSwap.animate({
                            opacity: options.opacity
                        }, 100),
                        tileToSwap.animate({
                            top: directionSecondaryY+"="+(totalY)+"px",
                            left: directionSecondaryX+"="+(totalX)+"px"
                        }, options.speed),
                        tileToSwap.animate({
                            opacity: "1"
                        }, 100)
                    ).done(function(){
                        currentTile.removeAttr('style');
                        tileToSwap.removeAttr('style');

                        if(navigateTileIndex > currentTileindex) {
                            insertTileAtIndex(currentTile, navigateTileIndex);
                            insertTileAtIndex(tileToSwap, currentTileindex);
                        } else {
                            insertTileAtIndex(tileToSwap, currentTileindex);
                            insertTileAtIndex(currentTile, navigateTileIndex);
                        }

                        options.decorateCallback.call(that, that.currentTileMover);
                        options.attachEventCallback.call(that, that.currentTileMover);
                    });
                }
            },

            _lockTiles: function() {
                if(!window.cordova) {
                    this.ui.tileContainer.sortable('disable');
                }
            },

            _unlockTiles: function() {
                if(!window.cordova) {
                    this.ui.tileContainer.sortable('enable');
                }
            },

            _updateTileOrder: function() {

               // this.collection.update(this.getTileModelsInOrder());

                var that = this;
                var currentGatewayDSN = App.getCurrentGatewayDSN();

                var groupNewOrderIDs = [];
                var newOrderGrpDeviceIDs = this.getTileModelsInOrder();
                _.each(newOrderGrpDeviceIDs, function(tile) {
                    groupNewOrderIDs.push(tile.referenceId);
                });
                console.info("New order of tile on dashboard:: ", JSON.stringify(newOrderGrpDeviceIDs));

                var returnObject = {};


                // Update tile order inside group

                var finalOrder = [];

                var collection = App.salusConnector.getSessionUser().getTileOrderForGateway(currentGatewayDSN);

                //console.info("Before collection models : ", JSON.stringify(collection.models));

                _.each(collection.models, function(currTile) {
                    if(_.where(finalOrder, currTile.attributes).length > 0) {
                        //Already present in final Array. Skip this
                    } else if (_.where(newOrderGrpDeviceIDs, currTile.attributes).length == 0) {
                        //Not a group device. Add it in final array
                        finalOrder.push(currTile.attributes);
                    } else if (_.where(newOrderGrpDeviceIDs, currTile.attributes).length  > 0) {
                        //Get all new order group devices and add in final array
                        _.each(newOrderGrpDeviceIDs, function(currGroupTile) {
                            finalOrder.push(currGroupTile);
                        });
                    }
                });

//                var returnObject = {};
//                Object.defineProperty(returnObject, currentGatewayDSN, {
//                    enumerable : true,
//                    value: finalOrder
//                });

				var fullTileOrder = App.salusConnector.getSessionUser().get("tileOrderCollection").toJSON();
				fullTileOrder = _.mapObject(fullTileOrder, function (order) {
					return order.map(function (tileModel) {
						return tileModel.pick("referenceType", "referenceId");
					});
				});
				fullTileOrder[currentGatewayDSN] = finalOrder;

                App.salusConnector.getSessionUser().saveTileOrder(fullTileOrder).then(function() {
                    var collection = App.salusConnector.getSessionUser().getTileOrderForGateway(currentGatewayDSN);
                    //console.info("Before -- collection models : ", JSON.stringify(collection.models));
                    collection.reset(finalOrder);
                    console.info("After updating collection models : ", JSON.stringify(collection.models));
                });

            },

            /**
             * look for the welcome tile and remove it
             * @private
             */
            _dismissWelcomeTile: function() {
                var that = this;
                // tiles = this.ui.tileContainer.packery("getItemElements");

                _.each(this.tiles, function(tile) {
                    var cid = $(tile)[0].cid,
                        tileView = that.children.findByCid(cid);
                    /* var cid = $(tile).data("view-cid"),
                         tileView = that.children.findByCid(cid);*/

                    if (tileView && tileView.model.get("referenceType") === "welcome") {
                        // that.ui.tileContainer.packery("remove", tileView.$el);
                        tileView.$el.remove();
                        // relayout packery
                        setTimeout(function() {
                            // that.ui.tileContainer.packery("layout");
                            App.salusConnector.unpinFromDashboard(null, "welcome");
                        }, 0);
                    }
                });
            },
            onBeforeDestroy: function() {
                // Fixes a bug where the drag mode backdrop would stick around when navigating
                // away from the page in the middle of a drag
                if (this.$tileDragBackdrop) {
                    this.$tileDragBackdrop.remove();
                }

                this.ui.tileContainer.off();
            },
            onDestroy: function() {
                if (this.addGatewayTile) {
                    this.addGatewayTile.destroy();
                }
            }
        });

        return Views.TileManagerView;
    });
});
