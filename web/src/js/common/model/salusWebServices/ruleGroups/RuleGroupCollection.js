"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/salusWebServices/ruleGroups/RuleGroup.model"
], function (App, P, AylaConfig, AylaBackedMixin) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleGroupCollection = B.Collection.extend({
			model: Models.RuleGroupModel,

			initialize: function (data, options) {
				if (options && options.dsn) {
					this.dsn = options.dsn;
				}
                
                this.gatewayKey=options.key ? options.key : null;

				this.listenTo(App.vent, "onLogin", this.setupGatewayNodeListener);
				this.listenTo(App.salusConnector.getDevice(this.dsn), "remove:rule", this.clearOutRule);
			},

			/**
			 * each ruleGroup will listen to its' gateway's node for a change
			 * on change, it will call setSelectedRuleGroup to set 'selected' property to matching key
			 */
			setupGatewayNodeListener: function () {
				var that = this, gateway = App.salusConnector.getDeviceByKey(this.gatewayKey);

				if (gateway) {
					var node = gateway.getGatewayNode();

					if (node) {
						node.getDevicePropertiesPromise().then(function () {
							// do it manually at first
							that.setSelectedRuleGroup();

							// setup listener for changes
							that.listenTo(node.get("MyStatus"), "change:value", that.setSelectedRuleGroup);
						});
					}
				}
			},

			setSelectedRuleGroup: function () {
				var gateway = App.getCurrentGateway(),
					node = gateway ? gateway.getGatewayNode() : false,
					selectedStatus = node && node.get("MyStatus") ?
						this.findWhere({key: node.getPropertyValue("MyStatus")}) : false;

				
				var previouslySelected = this.findWhere({selected: true});
				if (previouslySelected) {
					previouslySelected.set("selected", false);
				}
                
                if (selectedStatus) {
					selectedStatus.set("selected", true);
				} else if(node.get("MyStatus") && (!node.getPropertyValue("MyStatus"))){
                    var previouslySelected = this.findWhere({selected: true});

					if (previouslySelected) {
						previouslySelected.set("selected", false);
					}
                }
			},

			/**
			 * when removing a rule, search the ruleGroup for the rule key and get rid of it
			 * @param rule
			 */
			clearOutRule: function (rule) {
				var key = rule.get("key");

				this.each(function (ruleGroup) {
					var rules = ruleGroup.get("rules"), match;

					match = _.findWhere(rules, {key: key});

					if (match) {
						ruleGroup.set("rules", _.without(rules, match));
						ruleGroup.update();
					}
				});
			},

			refresh: function (isLowPriority) {
				var promise = this.load(isLowPriority);
				App.salusConnector.setDataLoadPromise("ruleGroups", promise);

				return promise;
			},

			load: function (isLowPriority) {
				var that = this,
					fetchURLModel = {dsn: this.dsn};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.ruleGroups.list)(fetchURLModel), null, "GET", null, {isLowPriority: isLowPriority}).then(function (data) {
					that.reset(data.rulesGroup_list, {
						parse: true,
						dsn: that.dsn
					});

					that.setSelectedRuleGroup();
				});
			},

			unregisterAll: function () {
				var that = this, urlModel = {dsn: this.dsn},
					url = _.template(AylaConfig.endpoints.ruleGroups.deleteAll)(urlModel);

				return App.salusConnector.makeAjaxCall(url, null, "DELETE", "text").then(function () {
					that.destroy();
				});
			},

			setDSN: function (dsn) {
				if (_.isString(dsn)) {
					this.dsn = dsn;
				}
			},

			isLoaded: function () {
				return !!this.length; //todo
			},

			getRuleGroupKeysForRule: function (ruleKey) {
				return this.filter(function (ruleGroup) {
					return !!_.findWhere(ruleGroup.get("rules"), {key: ruleKey});
				}).map(function (ruleGroup) {
					return ruleGroup.get("key");
				});
			}
		}).mixin([AylaBackedMixin], {
			apiWrapperObjectName: "rulesGroup_list"
		});
	});

	return App.Models.RuleModel;
});

