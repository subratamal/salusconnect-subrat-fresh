"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, templates, SalusViewMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {

		Views.StatusTableRow = Mn.ItemView.extend({
			className: "row status-row",
			template: templates["equipment/messageCenter/statusTableRow"]
		}).mixin([SalusViewMixin]);

		Views.DetailTableRow = Mn.ItemView.extend({
			className: "row detail-row",
			template: templates["equipment/messageCenter/detailTableRow"]
		}).mixin([SalusViewMixin]);

		// TODO Implement with backing model when available
		Views.CriteriaTableRow = Mn.ItemView.extend({
			className: "row criteria-row",
			template: templates["equipment/messageCenter/criteriaTableRow"],

			events: {
				"click .pencil-icon": "_onEdit"
			},

			onRender: function () {
				this.$(".bb-canned-text").html('<span>1. Text Message 415-555-4598</span>&nbsp&nbsp&nbsp<span class="color-light">if</span>&nbsp&nbsp&nbsp<span>Hallway upstairs CO alarm fires</span>&nbsp&nbsp&nbsp<span class="color-light">and</span>&nbsp&nbsp&nbsp<span>loses communication</span>');
			},

			_onEdit: function (e) {
				this._notImplementedAlert(e);
				//TODO: using this event to toggle alarm sounding
				this.model.set("alarmSounding", !this.model.get("alarmSounding"));
			}
		}).mixin([SalusViewMixin]);

		var tableTypeMap = {
			"status": {
				"titleKey": "messageCenter.alerts.currentStatus",
				"childView": Views.StatusTableRow
			},
			"detail": {
				"titleKey": "messageCenter.alerts.details",
				"childView": Views.DetailTableRow
			},
			"criteria": {
				"titleKey": "messageCenter.alerts.criteria",
				"childView": Views.CriteriaTableRow
			}
		};

		Views.AlertDetailCompositeTable = Mn.CompositeView.extend({
			className: "row",
			template: templates["equipment/messageCenter/alertDetailCompositeTable"],
			childViewContainer: ".bb-child-container",

			initialize: function (options) {
				if (!options || !options.tableType || !tableTypeMap[options.tableType]) {
					throw Error("AlertDetailCompositeTable requires a table type as a param that must exist in the table types map");
				}

				this.titleKey = tableTypeMap[options.tableType].titleKey;
				this.childView = tableTypeMap[options.tableType].childView;
			},

			onRender: function () {
				this.$(".bb-title-key-container").text(App.translate(this.titleKey));
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Equipment.Views;
});