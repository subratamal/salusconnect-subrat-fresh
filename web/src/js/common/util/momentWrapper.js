'use strict';

define([
	"app",
	"moment",
	"moment-timezone-with-data"
], function (App, momentJs, moment) {

	App.module("Utilities", function (Utils, App) {
		Utils.momentWrapper = function momentWrapper(arg1, arg2, chosenGateway, ingestInUTC) {
			var gateway, timeZone;

			if (chosenGateway && App.salusConnector ) {
				gateway = App.salusConnector.getDevice(chosenGateway);
				timeZone = !!gateway ? gateway.get("time_zone_string") : null;
			} else if (App && App.getCurrentGateway) {
				gateway = App.getCurrentGateway();
				timeZone = !!gateway ? gateway.get("time_zone_string") : null;
			}

			if (!gateway || !timeZone) {
				return moment(arg1, arg2);
			} else if (gateway && timeZone && !ingestInUTC) {
				return moment.tz(arg1, arg2, timeZone).local();
			} else if (gateway && timeZone) {
				return moment.tz(arg1, arg2, "Etc/UTC").tz(timeZone);
			}
		};
	});

	return App.Utilities.momentWrapper;
});
