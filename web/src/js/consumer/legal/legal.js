"use strict";

define([
    "app",
    "application/Router",
    "consumer/legal/legal.controller"
], function(App, AppRouter, Controller) {
    
    App.module("Consumer.Legal", function(Legal, App) {
        Legal.Router = AppRouter.extend({
            appRoutes: {
                "legal/privacy": "privacyPolicy",
                "legal/terms": "termsAndConditions"
            }
        });
        
        App.addInitializer(function() {
           Legal.router = new Legal.Router({
               controller: Controller
           }); 
        });
    });
    return App.Consumer.Legal;
});


