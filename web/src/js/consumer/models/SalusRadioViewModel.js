"use strict";

define([
	"app",
	"jquery",
	"underscore",
	"backbone"
], function (App, $, _, B) {

	App.module("Consumer.Models", function (Models) {
		Models.RadioViewModel = B.Model.extend({
			defaults: {
				name: "",
				value: "",
				labelTextKey: "",
				id: "",
				classes: "",
				isChecked: false,
                nonKeyLabel: null
			}
		});

		Models.RadioCollection = B.Collection.extend({
			model: App.Consumer.Models.RadioViewModel
		});
	});

	return App.Consumer.Models;
});
