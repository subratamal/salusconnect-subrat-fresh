"use strict";

define([
	"app",
	"momentWrapper",
	"md5",
    "aes"
], function (App, moment, md5) {
	var utilities = {};

	utilities.convertToTimestamp = function (date) {
		return moment(date).format("YYYY-MM-DDTHH:MM:SS.000Z");
	};

	utilities.setBackgroundImage = function ($el, backgroundUrl) {
		var backgroundString = "linear-gradient(180deg, rgba(0, 0, 0, 0) 50%, rgba(21, 21, 21, 1) 100%), url(" +
			App.rootPath(backgroundUrl || "") + ") center no-repeat";

		$el.css("background", backgroundString);
		$el.css("background-size", "cover");
		$el.css("background-position", "center");
	};

	utilities.md5HashString = function (string) {
		return md5(string);
	};

	utilities.stripNonAlphanumericChars = function (str) {
		return str.replace(/\W/g, '');
	};

	utilities.shortenText = function (text, maxLength) {
		// substr returns a new string
		if (!text) {
			return "";
		}
		return text.length > maxLength ? text.substr(0, maxLength) + "..." : text;
	};

	utilities.roundHalf = function (value) {
		return (Math.round(value * 2) / 2).toFixed(1);
	};

	utilities.appendEmailTemplateParameters = function (url, emailTemplateId, emailSubject) {
		var params = "?",
			environment = App.getEnvironment();

		if (environment && environment !== "prod") {
			emailTemplateId = emailTemplateId + "_" + environment;
		} else {
			var lang = App.i18n.lng();
			emailTemplateId = emailTemplateId + "_" + lang;
		}

		if (url.indexOf("?") > -1) {
			params = "&";
		}

		params += "email_template_id=" + encodeURIComponent(emailTemplateId);

		if (emailSubject) {
			params += "&email_subject=" + encodeURIComponent(emailSubject);
		}

		return url + params;
	};

	utilities.noOp = function () {
		// noOp
	};

	utilities.isInteger = Number.isInteger || function (value) {
        return typeof value === "number" &&
            isFinite(value) &&
            Math.floor(value) === value;
    };
    
    utilities.encryptWithAES = function(plainText, key, iv) {
        var srcs = CryptoJS.enc.Utf8.parse(plainText);
        var convertedKey  = CryptoJS.enc.Utf8.parse(key);
        var convertedIv   = CryptoJS.enc.Utf8.parse(iv);
        
        var encrypted = CryptoJS.AES.encrypt(srcs, convertedKey, {iv: convertedIv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
        return encrypted.ciphertext.toString();
    };
    
    utilities.decryptWithAES = function(cipherText, key, iv) {
        var encryptedHexStr = CryptoJS.enc.Hex.parse(cipherText);
        var srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr);
        var convertedKey  = CryptoJS.enc.Utf8.parse(key);
        var convertedIv   = CryptoJS.enc.Utf8.parse(iv);
        
        var decrypt = CryptoJS.AES.decrypt(srcs, convertedKey, {iv: convertedIv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
        var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8); 
        return decryptedStr.toString();
    };

	return utilities;
});