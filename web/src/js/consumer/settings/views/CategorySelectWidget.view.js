"use strict";

define([
	"momentWrapper",
	"app",
	"common/config",
	"common/constants",
	"bluebird",
	"collapse",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/consumer.views",
	"consumer/views/mixins/mixin.equipmentPanel",
	"consumer/models/SalusDropdownViewModel"
], function (moment, App, config, constants, P, collapse, consumerTemplates, SalusView, ConsumerViews) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn,  $, _) {

		Views.CategorySelectDeviceItem = Mn.ItemView.extend({
			template: consumerTemplates["settings/category/categoryDevice"],
			className: "row",
			initialize: function (options) {
				this.model = options.model;
			},
			templateHelpers: function () {
				return {
					currentName: this.model.getDisplayName()
				};
			}
		}).mixin([SalusView]);

		Views.CategorySelectView = Mn.CompositeView.extend({
			template: consumerTemplates["settings/category/categorySelect"],
			ui: {
				dropDown: ".bb-category-select-drop-down"
			},
			childViewContainer: ".bb-category-select-content",
			childView: Views.CategorySelectDeviceItem,
			initialize: function(options) {
				var that = this;
				this.sortedKeys = options.sortedKeys;
				this.equipmentByModelType = options.equipmentByModelType;
				this.selectedVal = 0;

				this.valueCollection = new App.Consumer.Models.DropdownItemViewCollection();
				_.each(this.sortedKeys, function (object, idx) {
					that.valueCollection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: idx,
						text: App.translate("equipment.names." + object)
					}));
				});

				this.collection = new B.Collection(this.equipmentByModelType[this.sortedKeys[this.selectedVal]]);
			},
			onRender: function () {
				if (this.valueCollection.length > 1) {

					this.deviceDropDown = new ConsumerViews.SalusDropdownView({
						collection: this.valueCollection,
						viewModel: new App.Consumer.Models.DropdownViewModel({
							id: "equipmentModelSelect",
							dropdownLabelText: this.sortedKeys.length > 0 ? App.translate("equipment.names." + this.sortedKeys[this.selectedVal]) : ""
						}),
						required: true,
						boundAttribute: "value",
						model: this.model
					});

					this.ui.dropDown.append(this.deviceDropDown.render().$el);
					this.listenTo(this.deviceDropDown ,"value:change", this.updateDeviceList);
				} else {
					this.$el.addClass("no-subcategory-dropdown");
				}
			},
			updateDeviceList: function (argsObj) {
				var selectedValTmp = argsObj.selectedValue;

				if (this.selectedVal !== selectedValTmp) {
					this.selectedVal = selectedValTmp;
					this.collection = new B.Collection(this.equipmentByModelType[this.sortedKeys[this.selectedVal]]);
					this.trigger("changed:selectedModel", {'selectedVal' : this.selectedVal});
					this.render();
				}
			},
			onDestroy: function () {
				this.deviceDropDown.destroy();
			}
		}).mixin([SalusView]);

		Views.CategorySelectWidget = Mn.LayoutView.extend({
			className: "equipment-settings-widget",
			ui: {
				title: ".bb-widget-header-text",
				iconLeft: ".bb-widget-header-icon-left",
				iconRight: ".bb-widget-header-icon-right",
				panelHeader: ".bb-widget-header",
				panel: ".panel"
			},
			initialize: function (options) {
				this.categoryData = {
					'sortedKeys': options.sortedKeys,
					'equipmentByModelType': options.equipmentByModelType
				};
			},
			onRender: function () {
				this._setupCategorySelectView();
			},
			_setupCategorySelectView: function () {
				this.categorySelectView = new Views.CategorySelectView(this.categoryData);
				this.listenTo(this.categorySelectView, "changed:selectedModel", function (argsObj){
					this.trigger("changed:selectedModel", argsObj);
				});
				this.contentRegion.show(this.categorySelectView);
			}
		}).mixin([App.Consumer.Views.Mixins.EquipmentPanel]);
	});

	return App.Consumer.Equipment.Views.CategorySelectWidget;
});