"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Equipment.Models", function (Models, App, B) {
		Models.MyEquipmentPageModel = B.Model.extend({
			defaults: {
				sortBy: "ALIAS",
				sortDesc: true
			},

			sortByEnum: {
				"ALIAS": "alias",
				"INSTALL_DATE": "connected_at",
				"STATUS": "connection_status"
			}
		});
	});

	return App.Consumer.Equipment.Models.MyEquipmentPageModel;
});