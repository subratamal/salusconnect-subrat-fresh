"use strict";

define([
	"app",
	"common/constants",
	"common/model/salusWebServices/rules/ruleMenus/RuleSubMenuCollection",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/salusWebServices/rules/ruleMenus/DelayedActionMenu",
	"common/model/salusWebServices/rules/ruleMenus/ActionMenu",
	"common/model/salusWebServices/rules/ruleMenus/ConditionMenu"
], function (App, constants ) {

	// this is to be used for populating the criteria menus
	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.OneTouchMenuManager = B.Model.extend({
			defaults: {
				conditionMenus: null,
				actionMenus: null,
				delayedActionMenu: null
			},
			depth: null, // how deep into the menu we are - needed to know when to close the menu
			initialize: function () {
				this.subMenuViewManager = new App.Models.OneTouchSubMenuManager();

				if (!this.get("conditionMenus") || !this.get("actionMenus") || !this.get("delayedActionMenu")) {
					this._buildMenus();
				}

				this.ruleMakerManager = App.Consumer.OneTouch.ruleMakerManager;

				this.depth = 0;
			},
			getMenu: function (menuType) {
				var menu;

				if (menuType === constants.oneTouchMenuTypes.condition) {
					menu = this.get("conditionMenus");
				} else if (menuType === constants.oneTouchMenuTypes.action) {
					menu = this.get("actionMenus");
				} else if (menuType === constants.oneTouchMenuTypes.delayedAction) {
					menu = this.get("delayedActionMenu");
				}

				return menu;
			},

			setDevice: function (device) {
				this.currentDevice = device;
			},

			addToRule: function (model, data) {
				if (model.get("type") === constants.oneTouchMenuTypes.condition) {
					this.ruleMakerManager.addCondition(data);
				} else if (model.get("type") === constants.oneTouchMenuTypes.action) {
					this.ruleMakerManager.addAction(data);
				} else if (model.get("type") === constants.oneTouchMenuTypes.delayedAction) {
					this.ruleMakerManager.addDelayedAction(data);
				}
			},

			/**
			 * check that the coming in ruleData is not a duplicate condition or action
			 * call when creating rule
			 * @param conditionOrAction
			 * @param ruleData
			 * @returns {boolean}
			 */
			nonDuplicateCriteria: function (conditionOrAction, ruleData) {
				var duplicate;

				if (conditionOrAction === constants.oneTouchMenuTypes.condition) {
					duplicate = this.ruleMakerManager.getConditions().find(function (condition) {
						return _.isEqual(condition.toJSON(), ruleData);
					});
				} else if (conditionOrAction === constants.oneTouchMenuTypes.action) {
					duplicate = this.ruleMakerManager.getActions().find(function (action) {
						return _.isEqual(action.toJSON(), ruleData);
					});
				} // todo: check delayed actions

				return !duplicate;
			},

			/**
			 * build the menus
			 * @private
			 */
			_buildMenus: function () {

				var rootConditions = new App.Models.ConditionMenu(this.subMenuViewManager);
				this.set("conditionMenus", rootConditions.get("menu"));

				var rootActions = new App.Models.ActionMenu(this.subMenuViewManager);
				this.set("actionMenus", rootActions.get("menu"));

				/* Begin delayed Action (then do this) menu */
				var rootDelayedActions = new App.Models.DelayedActionMenu(this.subMenuViewManager);
				this.set("delayedActionMenu", rootDelayedActions.get("menu"));
			}
		});

		Models.RuleMenuItemModel = B.Model.extend({
			defaults: {
				uniqueView: null, // if this things submenu has a special view
				displayTextKey: null,
				displayText: null,
				image: null, // menu image to the left of the text
				condition: null // apply some rules
			},

			initialize: function (data, options) {
				if (options) {
					this.childMenu = options.childMenu;
				}
			}
		});
	});

	return App.Models.OneTouchMenuManager;
});
