"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/widgets/StatusWidgetContent.view",
	"consumer/equipment/views/IT600ErrorModal.view",
	"consumer/views/SalusModal.view",
	"consumer/views/modal/ModalDefaultHeader.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.EquipmentStatusWidget = Mn.LayoutView.extend({
			template: consumerTemplates["equipment/widgets/equipmentStatusWidget"],
			className: "equipment-status-widget col-xs-12",
			regions: {
				state: ".bb-status-state", // icon placement and device-specific stuff
				table: ".bb-status-table" // collection view for events or group locations
			},
			bindings: {
				".bb-pin-device-icon": {
					observe: "isInDashboard",
					update: function ($el, val, model) {
                        var deviceType = this.model.modelType;
                        if(deviceType === constants.modelTypes.GATEWAY || deviceType === constants.modelTypes.IT600THERMOSTAT ||
                                deviceType === constants.modelTypes.WATERHEATER || deviceType === constants.modelTypes.SMARTPLUG ||
                                deviceType === constants.modelTypes.DOORMONITOR || deviceType === constants.modelTypes.WINDOWMONITOR ||
                                deviceType === constants.modelTypes.ENERGYMETER || deviceType === constants.modelTypes.THERMOSTAT ||
                                deviceType === constants.modelTypes.MINISMARTPLUG){
                                    var modelVal = model.get("isInDashboard");
                                    if (modelVal === null) {
                                        $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                                    } else if (modelVal === false) {
                                        $el.removeClass("hidden").removeClass("pin-icon").addClass("unpin-icon");
                                    } else if (modelVal === true) {
                                        $el.removeClass("hidden").removeClass("unpin-icon").addClass("pin-icon");
                                    }
                        }
                        else{
                            $el.removeClass("pin-icon").removeClass("unpin-icon").addClass("hidden");
                        }
						
					}
				},
				".bb-status-connection": {
					observe: "connection_status",
					update: function ($el, connection) {
						if (connection === "Online") {
							$el.addClass("connected");
						} else if (connection === "Offline" || connection === "Unknown" || 1) {
							$el.addClass("not-connected");
						}
					}
				}
			},
			ui: {
				connection: ".bb-status-connection",
				warning: ".bb-warning-icon",
                windowSensorRule: ".bb-window-sensor-icon"
			},
			events: {
				"click .bb-settings-icon": "_goToSettingsPage",
				"click .bb-pin-device-icon": "_pinUnpinDeviceToDashboard",
				"click .bb-warning-icon": "_goToWarningsPage",
				"click .bb-identify-icon": "toggleSetIndicator",
                "click .bb-window-sensor-icon": "_goToWindowSensorRulePage"
			},
			initialize: function () {
				_.bindAll(this, "toggleSetIndicator");
              
			},
			onRender: function () {
				// setIndicator for gateway is on coordinator
				var model = this.model.isGateway() ? this.model.getCoordinator() : null;

				// setup identify binding for default device model or a delegate
				this.addBinding(model, ".bb-identify-icon", {
					observe: "SetIndicator",
					onGet: function (prop) {
						return prop ? prop.getProperty() : prop;
					},
					update: function ($el, val) {
                        if(this.model.modelType === constants.modelTypes.WINDOWMONITOR || 
                                this.model.modelType === constants.modelTypes.DOORMONITOR){
                            $el.addClass("invisible");
                        }
                        else{
                            if (_.isArray(val)) {
							// eMeter case - if any are, toggle
                                $el.toggleClass("active", _.some(val, function (singleVal) { return !!singleVal; }));
                            } else {
                                $el.toggleClass("active", !!val);
                            }
                        }
						
					}
				});
                
                if(this.model.modelType === constants.modelTypes.IT600THERMOSTAT){
                    this.ui.windowSensorRule.removeClass("hidden");
                }

				switch (this.model.modelType) {
					case constants.modelTypes.CARBONMONOXIDEDETECTOR:
					case constants.modelTypes.SMOKEDETECTOR:
					case constants.modelTypes.WINDOWMONITOR:
					case constants.modelTypes.DOORMONITOR:
						// monitors
						this.state.show(new Views.StatusWidgetContentMonitoringDevice({
							model: this.model
						}));
						break;
					case constants.modelTypes.MINISMARTPLUG:
					case constants.modelTypes.SMARTPLUG:
					case constants.modelTypes.WATERHEATER:
						// smart plugs don't have connection indicator - they give "Unknown" connection
						this.ui.connection.hide();
						this.state.show(new Views.StatusWidgetContentWithSwitcher({
							model: this.model
						}));
						/*this.table.show(new Views.StatusWidgetCollectionView({
							type: "events"
						}));*/
						break;
					case constants.modelTypes.THERMOSTAT:
					case constants.modelTypes.IT600THERMOSTAT:
						// thermostat - also don't have connection indicator
						this.ui.connection.hide();
						this.state.show(new Views.StatusWidgetContentThermostat({
							model: this.model
						}));
						/*this.table.show(new Views.StatusWidgetCollectionView({
						 type: "events"
						 }));*/
						this.errorList = this._checkForErrors();
						if (this.errorList) {
							this.ui.warning.removeClass("hidden");
						}
                        
                        

						break;
					case constants.modelTypes.ENERGYMETER:
						this.state.show(new Views.StatusWidgetContentEnergyMeter({
							model: this.model
						}));
						/*this.table.show(new Views.StatusWidgetCollectionView({
						 type: "groups" // or is this zones
						 }));*/
						break;
					default:
						this.state.show(new Views.StatusWidgetContentGeneric({
							iconPath: this.model.get("equipment_page_icon_url")
						}));
						break;
				}
                 
			},
			_goToSettingsPage: function () {
				App.navigate("settings/myEquipment/" + this.model.get("dsn"));
			},
			_pinUnpinDeviceToDashboard: function () {
				if (this.model.get("isInDashboard")) {
					App.salusConnector.unpinFromDashboard(this.model, "device");
				} else {
					App.salusConnector.pinToDashboard(this.model, "device");
				}
			},
			_checkForErrors: function () {
				var propertyStringBase = "Error", amountOfPossibleErrors = 32, errors = [], numberString;

				for (var i = 1; i <= amountOfPossibleErrors; i++) {
					if (i < 10) {
						 numberString = "0" + i.toString();
					} else {
						numberString = i.toString();
					}

					var propertyString = propertyStringBase + numberString;

					if (this.model.get(propertyString)) {
						var value = this.model.getPropertyValue(propertyString);

						if (!!value) {
							errors.push({error: propertyString});
						}
					}
				}

				return errors.length ? errors : false;
			},
			_goToWarningsPage: function () {
				var that = this;

				App.modalRegion.show(new App.Consumer.Views.SalusModalView({
					size: "modal-lg",
					headerView: new App.Consumer.Views.ModalDefaultHeader(),
					contentView: new App.Consumer.Equipment.Views.Modal.IT600ErrorModalView({errors: that.errorList})
				}));

				App.showModal();
			},
			toggleSetIndicator: function () {
				if (this.model.isGateway()) {
					this.model.getCoordinator().toggleSetIndicator();
				} else {
					this.model.toggleSetIndicator();
				}
			},
            _goToWindowSensorRulePage: function (){
                App.navigate("oneTouch/deviceWindowSensor/"+this.model.get("key"));
            },
			onBeforeDestroy: function () {
				var model = this.model.isGateway() ? this.model.getCoordinator() : this.model;
                
                if (model) {
                    //Bug_CASBW-216 start
                    var leaveNetworkObj = this.model.get("LeaveNetwork");
                    if(leaveNetworkObj){
                        if(leaveNetworkObj.get("getterProperty") !== undefined){
                            leaveNetworkObj = leaveNetworkObj.get("getterProperty"); 
                         }else{
                            leaveNetworkObj = leaveNetworkObj.models[0].get("getterProperty");
                         }

                         if(leaveNetworkObj.get("value") === 0){
                            model.undoSetIndicator();
                         }
                    }else{
                        model.undoSetIndicator();
                    }
                    
                    //Bug_CASBW-216 end
//                    if (model) {
//                       model.undoSetIndicator();
//                    }
                }
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.EquipmentStatusWidget;
});