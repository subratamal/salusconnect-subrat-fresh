"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/consumer.views",
	"consumer/login/views/ForgotPasswordConfirmationModal.view"
], function (App, consumerTemplates, SalusViewMixin, ConsumerViews, ForgotPasswordConfirmationModalView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.ForgotPasswordWell = Mn.LayoutView.extend({
			id: "bb-forgot-password-content-well",
			className: "forgot-password-content-well",
			template: consumerTemplates["login/ForgotPassword"],
			regions: {
				contentRegion: "#email-password-btn-region"
			},
			ui: {
				"bbEmailInputPlaceholder": ".bb-email-input-el-placeholder"
			},
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "handleButtonClicked");

				this.emailBox = new ConsumerViews.SalusTextBox({
					inputClass: "email-form-box",
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2",
					iconPath: App.rootPath("/images/icons/icon_mail_blue.svg"),
					iconAlt: App.translate("login.forgotPassword.inputLabel"),
					inputPlaceholder: App.translate("login.forgotPassword.inputLabel"),
					inputType: "email",
					isRequired: true
				});

				this.emailPasswordButton = new ConsumerViews.SalusButtonPrimaryView({
					id: "email-password-btn",
					buttonTextKey: "login.forgotPassword.button",
					clickedDelegate: this.handleButtonClicked
				});
			},
			onRender: function () {
				this.ui.bbEmailInputPlaceholder.replaceWith(this.emailBox.render().$el);
				this.contentRegion.show(this.emailPasswordButton);
			},
			onDestroy: function () {
				this.emailBox.destroy();
			},
			goToChangePassword: function () {
				App.navigate("login/changePasswordWithToken");
			},
			/**
			 * Trigger the form submit when the enter key is pressed
			 */
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.handleButtonClicked();
				}
			},
			handleButtonClicked: function () {
				var loginPromise, emailAddress = this.$(".form-control").val(),
					that = this;

				this.model.set("user_email", emailAddress);

				if (App.validate.email(emailAddress)) {
					that.emailBox.clearError();
					loginPromise = App.salusConnector.sendResetPasswordToken(emailAddress);

					loginPromise.then(function () {
						that.showForgotPasswordConfirmationModal();
					}).catch(function (fail) {
						if (fail.status === 422) {
							//Check for "The email address has not signed up for an account." and display it if found
							var response = JSON.parse(fail.responseText);

							if (response.errors && response.errors.email && _.where(response.errors.email, "not found")) {
								that.emailBox.registerError(App.translate("common.validation.email.notFound"));
							} else if (response.errors && _.where(response.errors, "confirm your account")) {
								that.emailBox.registerError(App.translate("common.validation.ayla.confirmaccountbeforecontinue"));
							}
						}
					});
				} else {
					// Display an error if the email address format is incorrect or the field is blank
					that.emailBox.registerError(App.translate("common.validation.email.notValid"));
				}
			},
			showForgotPasswordConfirmationModal: function () {
				App.modalRegion.show(new ConsumerViews.SalusModalView({
					contentView: new ForgotPasswordConfirmationModalView({ model: this.model })
				}));

				App.showModal();
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.ForgotPasswordWell;
});
