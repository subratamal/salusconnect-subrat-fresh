"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusButtonPrimary.view"
], function (App, constants, consumerTemplates, SalusPage) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.OneTouchPinPageView = Mn.LayoutView.extend({
			id: "one-touch-pin-page",
			className: "container",
			template: consumerTemplates["oneTouch/oneTouchPinPage"],
			regions: {
				saveButtonRegion: ".bb-save-button",
				cancelButtonRegion: ".bb-cancel-button"
			},
			ui: {
				pinSelected: ".bb-pin-selected",
				unpinSelected: ".bb-unpin-selected"
			},
			events: {
				"click .bb-pin-tile": "_handlePinClick",
				"click .bb-unpin-tile": "_handleUnpinClick"
			},
			initialize: function () {
				_.bindAll(this, "handleCancelClicked", "_handlePinClick", "_handleUnpinClick", "handleSaveClicked");

				this.pin = false;
				this.currentRule = App.Consumer.OneTouch.ruleMakerManager.getCurrentRule();

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default cancel-button",
					clickedDelegate: this.handleCancelClicked
				});

				this.saveButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.save",
					classes: "col-xs-6",
					className: "width100 btn btn-primary save-button",
					clickedDelegate: this.handleSaveClicked
				});
			},

			onRender: function () {
				if (!this.currentRule) {
					return App.navigate("oneTouch");
				}

				this.ui.unpinSelected.addClass("selected");

				this.saveButtonRegion.show(this.saveButton);
				this.cancelButtonRegion.show(this.cancelButton);
			},

			handleCancelClicked: function () {
				window.history.back();
			},

			handleSaveClicked: function () {
				if (this.pin && this.currentRule) {
					this.currentRule.pinToDashboard();
				}

				App.navigate("oneTouch");
			},

			_handlePinClick: function () {
				this.pin = true;

				this.ui.pinSelected.addClass("selected");
				this.ui.unpinSelected.removeClass("selected");
			},
			
			_handleUnpinClick: function () {
				this.pin = false;

				this.ui.pinSelected.removeClass("selected");
				this.ui.unpinSelected.addClass("selected");
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "pin"
		});
	});

	return App.Consumer.OneTouch.Views.OneTouchPinPageView;
});

