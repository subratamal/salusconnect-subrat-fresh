"use strict";

define([
	"app",
	"consumer/setupWizard/views/SetupGatewayPage.view",
	"consumer/setupWizard/views/SetupLandingPage.view"
], function (App) {

	return App.Consumer.SetupWizard.Views;
});