"use strict";

define([
], function () {

	if (typeof String.prototype.endsWith !== 'function') {
		String.prototype.endsWith = function (suffix) { // jshint ignore:line
			return this.indexOf(suffix, this.length - suffix.length) !== -1;
		};
	}
});