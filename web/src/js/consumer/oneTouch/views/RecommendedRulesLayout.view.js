"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"common/model/salusWebServices/rules/recommendedRules",
	"consumer/oneTouch/views/EquipmentListModal.view",
	"consumer/views/SalusModal.view",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/salusWebServices/rules/RuleDetails.model"
], function (App, constants, consumerTemplates, SalusView, SalusButtonPrimary, ruleMap) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * layoutView will hold title
		 * has a region for the sections (smart plugs, thermostats)
		 */
		Views.RecommendedRulesLayoutView = Mn.LayoutView.extend({
			className: "recommended-rules-layout",
			template: consumerTemplates["oneTouch/recommendedRulesLayout"],
			regions: {
				sectionsRegion: ".bb-recommended-sections-region",
				addButton: ".bb-add-button",
				cancelButton: ".bb-cancel-button"
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "handleAddClick", "handleCancelClick", "_findClickedRuleModel");

				this.sectionsCollectionView = new Views.RecommendedRulesCollectionView();

				this.listenTo(this.sectionsCollectionView, "click:oneTouch", function () {
					that.addButtonView.enable();
				});

				this.addButtonView = new SalusButtonPrimary({
					id: "add-button",
					classes: "btn-primary width100 disabled preserve-case",
					buttonTextKey: "equipment.oneTouch.recommended.addRecommended",
					clickedDelegate: this.handleAddClick
				});

				this.cancelButtonView = new SalusButtonPrimary({
					id: "cancel-button",
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});
			},
			onRender: function () {
				this.sectionsRegion.show(this.sectionsCollectionView);
				this.addButton.show(this.addButtonView);
				this.cancelButton.show(this.cancelButtonView);
			},
			handleAddClick: function () {
				var recommendedRule = this._findClickedRuleModel();

				if (recommendedRule) {
					var contentView = new App.Consumer.OneTouch.Views.EquipmentListModal({
						rule: recommendedRule.rule,
						deviceType: recommendedRule.modelType
					});

					// on save deselect all
					this.listenTo(contentView, "deselect:rules", this.handleCancelClick);

					App.modalRegion.show(new App.Consumer.Views.SalusModalView({
						size: "modal-lg",
						contentView: contentView
					}));

					App.showModal();
				}
			},
			handleCancelClick: function () {
				this.sectionsCollectionView.setSelectedOff();
				this.addButtonView.disable();
			},
			_findClickedRuleModel: function () {
				var recommendedRule = {
					rule: {},
					modelType: ""
				};

				// go through sections and childViews
				this.sectionsCollectionView.children.some(function (compositeView) {
					return compositeView.children.some(function (childView) {
						if (childView.ui.selected.hasClass("selected")) {
							recommendedRule.rule = childView.model;
							recommendedRule.modelType = compositeView.model.get("modelType");
							return true;
						}

						return false;
					});
				});

				return !!recommendedRule.rule ? recommendedRule : false;
			}
		}).mixin([SalusView]);

		/**
		 * individual recommended rule
		 */
		Views.RecommendedRuleItemView = Mn.ItemView.extend({
			className: "recommended-rule col-xs-12 col-sm-6",
			template: consumerTemplates["oneTouch/recommendedRule"],
			ui: {
				name: ".bb-one-touch-name",
				description: ".bb-one-touch-description",
				selected: ".bb-rule-selected"
			},
			events: {
				"click .bb-one-touch-icon": "handleIconClick"
			},
			onRender: function () {
				this.ui.name.text(App.translate(this.model.get("name")));
				this.ui.description.text(App.translate(this.model.get("description")));
			},
			handleIconClick: function (/*event*/) {
				this.trigger("click:oneTouch", this);
				this.ui.selected.addClass("selected");
			}
		}).mixin([SalusView]);

		/**
		 * composite displays section title and holds individual rules
		 */
		Views.RecommendedRulesCompositeView = Mn.CompositeView.extend({
			className: "recommended-rules-composite col-xs-12",
			template: consumerTemplates["oneTouch/recommendedRulesComposite"],
			childView: Views.RecommendedRuleItemView,
			childViewContainer: ".bb-recommended-rules-container",
			childEvents: {
				"click:oneTouch": "triggerOneTouchClick"
			},
			ui: {
				equipmentType: ".bb-equipment-type"
			},
			initialize: function () {
				_.bindAll(this, "triggerOneTouchClick");

				this.collection = new B.Collection(ruleMap[this.model.get("modelType")]);
			},
			onRender: function () {
				var key = "equipment.oneTouch.recommended." + this.model.get("modelType") + ".title";
				this.ui.equipmentType.text(App.translate(key));
			},
			triggerOneTouchClick: function (/*clickedView*/) {
				this.trigger("click:oneTouch");
			}
		}).mixin([SalusView]);

		/**
		 * collectionView just manages sections and shows a composite
		 */
		Views.RecommendedRulesCollectionView = Mn.CollectionView.extend({
			className: "recommended-rules-collection row",
			template: false,
			childView: Views.RecommendedRulesCompositeView,
			childEvents: {
				"click:oneTouch": "setSelectedOff"
			},
			initialize: function () {
				this.collection = new B.Collection();

				if (App.salusConnector.hasSmartPlugs()) {
					this.collection.push({modelType: constants.modelTypes.SMARTPLUG});
				}

				if (App.salusConnector.hasThermostats()) {
					this.collection.push({modelType: constants.modelTypes.THERMOSTAT});
				}
                
                if (App.salusConnector.hasIt600Thermostats()) {
					this.collection.push({modelType: constants.modelTypes.IT600THERMOSTAT});
				}

				if (App.salusConnector.hasWaterHeaters()) {
					this.collection.push({modelType: constants.modelTypes.WATERHEATER});
				}
			},
			setSelectedOff: function () {
				// once a recommended rule is clicked, set it to be the only one clicked
				this.children.each(function (collectionView) {
					collectionView.children.each(function (childView) {
						childView.ui.selected.removeClass("selected");
					});
				});

				this.trigger("click:oneTouch");
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.RecommendedRulesLayoutView;
});