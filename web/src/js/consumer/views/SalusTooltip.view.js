"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusView",
	"popover"
], function (App, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {

		Views.SalusTooltipView = Mn.ItemView.extend({
			template: false,
			className: "info",
			initialize: function () {
				var tooltipOptions = {
					title: App.translate(this.model.get("title")),
					content: App.translate(this.model.get("content")),
					trigger: "hover click",
					placement: "bottom"
				};

				this.$el.popover(tooltipOptions);
			}
		}).mixin([SalusViewMixin]);

	});

	return App.Consumer.Views.SalusTooltipView;
});
