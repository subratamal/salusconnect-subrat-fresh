"use strict";

define([
	"app",
	"common/config",
	"common/constants",
	"underscore",
	"bluebird",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice",
	"common/model/ayla/Address",
	"common/model/salusWebServices/ThermostatSingleControlCollection",
	"common/model/salusWebServices/rules/RuleCollection",
	"common/model/salusWebServices/ruleGroups/RuleGroupCollection"
], function (App, config, constants, _, P, AylaConfig, BaseAylaDevice, AylaAddress) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for  ayla gateway device. it allows us to construct one and run unit tests and such
		 */
		Models.GatewayAylaDevice = B.Model.extend({
			modelType: constants.modelTypes.GATEWAY,
			tileType: "gateway",
			defaults: {
				// get properties
				GatewaySoftwareVersion: null, //string length 10
				GatewayHardwareVersion: null, //string length 10
				NetworkWiFiMAC: null, //string wifi mac
				NetworkWiFiIP: null, //string wifi ipv4
				NetworkLANMAC: null, //string lan mac
				NetworkLANIP: null, //string lan ipv4
				NetworkSSID: null, //string wifi ssid
				NetworkPassword: null, //string wifi network password TBC: Scramble required?
				ErrorGatewayUART: null, //boolean Reported when coordinator ZigBee UART error
				weather: null, //weather model
				tscCollection: null,//each gateway has a collection of TSCs

				connection_status: null,	// enum? : "Online" | ??

				// set properties
				SetRuleURL: null, // string
				SetRuleTrigger: null, // string

				//Meta Properties
				photo_url: null,
                   
                timeZone: null,
				time_zone_string: null,
				utc_offset: null
			},
			initialize: function (/*data, options*/) {
				_.bindAll(this, "invalidateWeather", "loadTimeZone", "getDevicesToPair", "pairDevice", "_onUnregistered", "_onBeforeUnregister", "updateTimeZone");

				// only do this for gateways!!!
				this.set("address", new AylaAddress({
					deviceId: this.get("key")
				}));

				this.listenTo(this, 'change:key', this._onKeyChanged, this);
				this.listenTo(this.get("address"), "change", this._onKeyChanged);

				this.set("tscCollection", new App.Models.ThermostatSingleControlCollection(null, {
					dsn: this.get("dsn")
				}));

				this.set("ruleCollection", new App.Models.RuleCollection(null, {
					dsn: this.get("dsn")
				}));

				this.set("ruleGroupCollection", new App.Models.RuleGroupCollection(null, {
					dsn: this.get("dsn"),
                    key: this.get("key")
				}));

				this.pairingDevices = [];

				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: null, // Gateways do not have a category id
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_wifi.svg"), // TODO this is just a placeholder and may change later
					device_category_type: constants.categoryTypes.GATEWAYS,
					device_category_name_key: "equipment.myEquipment.categories.types.gateways"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_wifi_grey.svg")
				});
			},

			initiatePairingMode: function () {
				var coordinator = this.getCoordinator();

				if (coordinator) {
					return coordinator.initiatePairingMode();
				} else {
					return P.reject("No coordinator to initiate pairing");
				}
			},

			deactivatePairingMode: function () {
				var coordinator = this.getCoordinator();

				if (coordinator) {
					return coordinator.deactivatePairingMode();
				} else {
					return P.reject("No coordinator to deactivate pairing");
				}
			},

			getDevicesToPair: function () {
				var url = _.template(AylaConfig.endpoints.device.register)(this.toJSON());

				// get all nodes
				return App.salusConnector.makeAjaxCall(url, null, "GET", null, null, [404]).then(function (nodes) {
					return _.map(nodes, function (node) {
						return node.device;
					});
				});
			},

			pairDevice: function (deviceDSN) {
				var that = this,
					payload = {
						device: {
							dsn: deviceDSN
						}
					};

				// post the device
				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.device.add, payload, "POST").then(function (data) {
					// keep track temporarily
					that.pairingDevices.push(data.device);

					return data;
				});
			},

			aParse: function (data) {
				var attrs = data;

				if (this.has("address")) {
					attrs.address = this.get("address").set(data.address);
				} else {
					attrs.address = new AylaAddress(data.address);
				}

				return attrs;
			},

			_onKeyChanged: function () {
				var address = this.get("address"),
					tscCollection = this.get("tscCollection"),
						ruleCollection = this.get("ruleCollection"),
						ruleGroupCollection = this.get("ruleGroupCollection");

				if (address) {
					address.set("deviceId", this.get("key"));
				}

				if (tscCollection.dsn !== this.get("dsn")) {
					tscCollection.setDSN(this.get("dsn"));
				}

				if (ruleCollection.dsn !== this.get("dsn")) {
					ruleCollection.setDSN(this.get("dsn"));
				}

				if (ruleGroupCollection.dsn !== this.get("dsn")) {
					ruleGroupCollection.setDSN(this.get("dsn"));
				}
			},

			/**
			 * TODO: this will be a GW only method
			 * TODO: implement
			 */
			openGatewayRegistration: function () {
				var url = AylaConfig.endpoints.device.registerButtonPush;
                var data = App.salusConnector.makeAjaxCall(url, null, "GET", null, null, [404]);

				return data;
			},
            
			registerGateway: function () {
				var that = this;
                return that.add()
						.then(that.getDevicesToPair)
						.catch(function (error) {
							if (error && error.status === 404) {
								//If we get to this point we added the gateway but failed to add the cordiantor and node.
								//So just unregister and let the user try again.
								that.unregister();
								return P.reject("GatewayNodesNotFound");
							}

							return P.reject(error);
						})
						.then(function (devices) {
							var promises = [];

							if (!devices || devices.length === 0) {
								App.warn("Registering Gateway without nodes.");
							}

							_.each(devices, function (device) {
								var dsn = device.dsn;
                                var oem_model = device.oem_model;
								if (dsn && (oem_model === "SAU2AG1-GW" || oem_model === "SAU2AG1-ZC")) {
									promises.push(that.pairDevice(dsn));
								}
							});
                            
                           // promises.push(App.salusConnector.getFullDeviceCollection().load());

							return P.all(promises).then(function () {
                                return P.all(App.salusConnector.getFullDeviceCollection().load()).then(function(){
                                    that.pairingDevices = []; //reset paringDevices side effect from using pairDevice method/

                                    // changing the current gatweay will trigger a filtered refresh of our new gateways devices
                                    App.salusConnector.changeCurrentGateway(that);

                                    return App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
                                        var gatewayNode = that.getGatewayNode(), coordinator = that.getCoordinator(), refreshPromises = [];

                                        if (gatewayNode) {
                                            refreshPromises.push(gatewayNode.getDevicePropertiesPromise().then(function () {
                                                return gatewayNode.setProperty("SetRefresh", 1);
                                            }));
                                        }

                                        if (coordinator) {
                                            refreshPromises.push(coordinator.getDevicePropertiesPromise().then(function () {
                                                //whether SetRefresh_d is success, must be SetIndicator to 0
                                                coordinator.setProperty("SetRefresh_d", 1);
                                                return coordinator.setProperty("SetIndicator", 0);
                                                //return coordinator.setProperty("SetRefresh_d", 1).then(function () {
                                                //	return coordinator.setProperty("SetIndicator", 0);
                                                //});
                                            }));
                                        }

                                        return P.all(refreshPromises).then(that.loadTimeZone);
                                    });
                                });
//								that.pairingDevices = []; //reset paringDevices side effect from using pairDevice method/
//
//								// changing the current gatweay will trigger a filtered refresh of our new gateways devices
//								App.salusConnector.changeCurrentGateway(that);
//
//								return App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
//									var gatewayNode = that.getGatewayNode(), coordinator = that.getCoordinator(), refreshPromises = [];
//
//									if (gatewayNode) {
//										refreshPromises.push(gatewayNode.getDevicePropertiesPromise().then(function () {
//											return gatewayNode.setProperty("SetRefresh", 1);
//										}));
//									}
//
//									if (coordinator) {
//										refreshPromises.push(coordinator.getDevicePropertiesPromise().then(function () {
//                                            //whether SetRefresh_d is success, must be SetIndicator to 0
//                                            coordinator.setProperty("SetRefresh_d", 1);
//                                            return coordinator.setProperty("SetIndicator", 0);
//											//return coordinator.setProperty("SetRefresh_d", 1).then(function () {
//											//	return coordinator.setProperty("SetIndicator", 0);
//											//});
//										}));
//									}
//
//									return P.all(refreshPromises).then(that.loadTimeZone);
//								});
							});
						});
			},

			_setDeviceJsonData: function (devData) {
				var that = this,
					address = this.get("address");

				this.set(devData.device);

				if (address) {
					return address.persist().then(function () {
						return devData;
					}).catch(function (error) {
						App.log("Error persisting address..");
						that.unregister();
						return P.reject(error);
					});
				} else {
					return devData;
				}
			},

			_onCreated: function () {
				this.refresh();

				App.vent.trigger("gateway:added", this);
			},

			_onBeforeUnregister: function () {
				var promises = [];

				// unregister metadata
				promises.push(this._unregisterDeviceMetaData());

				// ok if fail
				return P.all(promises.map(function (promise) {
					return promise.reflect();
				}));
			},

			_onUnregistered: function (/*data*/) {
				var promises = [],
						tileOrderCollection = App.salusConnector.getSessionUser().get("tileOrderCollection"), // which is actually a model
						dsn = this.get("dsn");

				// cleanup tile order
				if (tileOrderCollection.get(dsn)) {
					tileOrderCollection.unset(dsn);
					promises.push(tileOrderCollection.save());
				}

				// cleanup rules
				if (this.get("ruleCollection") && !this.get("ruleCollection").isEmpty()) {
					promises.push(this.get("ruleCollection").unregisterAll());
				}

				// cleanup status'
				if (this.get("ruleGroupCollection") && !this.get("ruleGroupCollection").isEmpty()) {
					promises.push(this.get("ruleGroupCollection").unregisterAll());
				}

				// tsc collection
				if (this.get("tscCollection") && !this.get("tscCollection").isEmpty()) {
					promises.push(this.get("tscCollection").unregisterAll());
				}

				// TODO: any more services that are associated with gateway dsn

				// attempt to fulfill and resolve all promises.
				// we don't really mind if they get rejected, hence promise.reflect();
				return P.all(promises.map(function (promise) {
					return promise.reflect();
				})).then(function () {
					return App.salusConnector.getFullDeviceCollection().load();
				});
			},

			getWeather: function (isLowPriority) {
				var that = this,
					weatherModel = this.get("weather");

				if (this.get("unregistered") === true) {
					return P.rejected("device unregistered");
				}

				if (weatherModel && weatherModel.valid && !this.get("timeToCheckWeather")) {
					return P.resolve(weatherModel);
				} else {
					return App.salusConnector.getWeatherForDevice(this, isLowPriority).then(function (weather) {
						weather.set("deviceId", that.get("key"));
						weather.valid = true;
						that.set("weather", weather);
						that.set("timeToCheckWeather", false);
						setTimeout(that.invalidateWeather, 900000);
						return weather;
					}).catch(function () {
						that.set("timeToCheckWeather", false); //Prevent another request on 404 or 500 errors.
						setTimeout(that.invalidateWeather, 900000);
						return  P.resolve(weatherModel);
					});
				}
			},

			invalidateWeather: function () {
				this.get("weather").valid = false;
				this.set("timeToCheckWeather",true);
			},

			/**
			 * Gets the gateway photo url from the ayla metadata api
			 * @returns {Promise}
			 */
			fetchPhotoUrl: function () {
				if (this.get("unregistered") === true) {
					return P.rejected("device unregistered");
				}

				var templateParams = {
					dsn: this.get("dsn"),
					propKey: "photourl"
				};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.metaDataApi.fetch)(templateParams), null, "GET", null, null, [404]);
			},

			/**
			 * delegate devices
			 */
			getGatewayNode: function () {
				var gatewaysDevices = App.salusConnector.getDevicesOnGateway(this);

				var node = gatewaysDevices.findWhereByType(constants.modelTypes.GATEWAYNODE);

				gatewaysDevices.destroy();

				return node;
			},

			getGatewayNodeDSN: function () {
				var node = this.getGatewayNode();

				return node ? node.get("dsn") : false;
			},

			getCoordinator: function () {
				var gatewaysDevices = App.salusConnector.getDevicesOnGateway(this);

				return gatewaysDevices.findWhereByType(constants.modelTypes.COORDINATORDEVICE);
			},

			getCoordinatorDSN: function () {
				var coordinator = this.getCoordinator();

				return coordinator ? coordinator.get("dsn") : false;
			},

			/**
			 * Deletes the gateway photo url metadata key and value.
			 * @returns {Promise}
			 */
			deletePhotoUrl: function () {
				if (this.get("unregistered") === true) {
					return P.rejected("device unregistered");
				}

				var templateParams = {
					dsn: this.get("dsn"),
					propKey: "photourl"
				};

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.metaDataApi.delete)(templateParams), null, "DELETE", "text", null, [404]);
			},

			loadTimeZone: function () {
				var that = this,
						url = _.template(AylaConfig.endpoints.device.timeZones.fetch)({ key: this.get("key")});
				return App.salusConnector.makeAjaxCall(url, null, "GET", "json", {isLowPriority: true}).then(function (timeZone) {
					that.set("time_zone_string", timeZone.time_zone.tz_id);
					that.set("utc_offset", timeZone.time_zone.utc_offset);
                    // Bug_SCS-3286	Use start instead of in-range for schedule
                    that.set("utc_dst", timeZone.time_zone.dst);
				});
			},
            getTimeZone: function (timeZone) {
                var that = this,
                    url = _.template(AylaConfig.endpoints.device.timeZones.detail)({key: App.translate(timeZone)});
                return App.salusConnector.makeAjaxCall(url, null, "GET", "json").then(function(tz) {
                    _.extend(tz, {tz_id: App.translate(timeZone)});
                    that.set("tz", tz);
                });
            },
            
            // Bug_CASBW-28 Start
            updateTimeZone: function() {
                var that = this, timeZone,
                    address = this.get("address"),
                    node = this.getGatewayNode(),
                    url = _.template(AylaConfig.endpoints.device.timeZones.fetch)({key: this.get("key")});
                if(address && address.get("country") === "us") {
                    timeZone = config.timeZonePrefix_us + "." + this.get("time_zone");
                } else {
                    timeZone = config.timeZonePrefix + "." + this.get("time_zone");
                }
                return this.getTimeZone(timeZone).then(function() {
                    return App.salusConnector.makeAjaxCall(url, that.get("tz"), "PUT", "json").then(function() {
                        return node.get("TimeZone").setProperty(App.translate(timeZone));
                    });
                });
            },
            // Bug_CASBW-28 End
                      
			setHasPhoto: function (value) {
				this._hasPhoto = value;
			},

			hasPhoto: function () {
				return this._hasPhoto;
			},

			_setTimeZoneProperty: function () {
				var gatewayNodeTZProp = this.getGatewayNode() ? this.getGatewayNode().get("TimeZone") : null;

				// if there is a timeZone property on the gateway node we will set it
				if (gatewayNodeTZProp) {
					var defaultTZ = App.getIsEU() ? config.euDefaultTimezone : config.usDefaultTimezone,
							tzString = this.get("time_zone_string");
					if (tzString && gatewayNodeTZProp.getProperty() !== tzString) {
						//if the ayla timzone called returned something and it is different from the node. set it
						return gatewayNodeTZProp.setProperty(tzString);
					} else if (!tzString) {
						// else set the default time zone
						return gatewayNodeTZProp.setProperty(defaultTZ);
					}
				}
			}
		}).mixin([BaseAylaDevice], {
			localOnlyProperties: ["weather", "time_zone_string", "utc_offset"]
		});
	});

	return App.Models.GatewayAylaDevice;
});
