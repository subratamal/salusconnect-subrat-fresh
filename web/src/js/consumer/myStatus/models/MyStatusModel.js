"use strict";

/**
 * This model is for all dashboard tiles
 */
define([
	"app",
	"bluebird",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked"
], function (App, P, constants, AylaConfig, AylaBacked) {

	App.module("Consumer.MyStatus.Models", function (Models, App, B) {
		Models.MyStatusModel = B.Model.extend({
			defaults: {
				key: null,
				name: null,
				icon: null,
				rules: null,

				//visual
				iconPath: null,
				topColor: null,
				botColor: null,
				selected: false
			},

			initialize: function (options) {
				this.ruleGroup = options.ruleGroup;
			}
		}).mixin([AylaBacked], {
			fetchUrl: AylaConfig.endpoints.ruleGroups.fetch
		});
	});

	return App.Consumer.MyStatus.Models.MyStatusModel;
});


