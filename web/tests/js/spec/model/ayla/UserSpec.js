/*jslint jasmine: true */
/**
 * test salus and ayla connectors
 */
define([
	"underscore",
	"spec/SpecHelper",
	"common/AylaConfig",
	"common/model/api/SalusConnector",
	"common/model/ayla/User"
], function (_, SpecHelper, AylaConfig, SaluConnector, SalusUser) {
	'use strict';

	describe("Ayla User ", function () {
		describe("When saving", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect calling persist() to post save to sever properly", function (done) {
				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.userProfile.save,
					function (proto, url, verb, data, success, fail, raw) {
						expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
						expect(verb).toBe("PUT");
						expect(data).toBeTruthy();

						var pData = JSON.parse(data);
						expect(pData).toBeTruthy();
						expect(pData.user).toBeTruthy();
						expect(_.keys(pData).length).toBe(1);

						var user = pData.user;
						expect(user.approved).toBe(true);
						expect(user.city).toBe("cty4");
						expect(user.company).toBe("company5");
						expect(user.confirmed_at).toBe("2015-08-11T18:15:33");
						expect(user.country).toBe("USA-");
						expect(user.created_at).toBe("2015-06-11T18:14:42Z");
						expect(user.email).toBe("jesse@digitalfoundry.com");
						expect(user.firstname).toBe("Jesse-t");
						expect(user.lastname).toBe("Shaver-t");
						expect(user.phone_country_code).toBe("01235");
						expect(user.phone).toBe("457855");
						expect(user.state).toBe("state4");
						expect(user.street).toBe("street7");
						expect(user.updated_at).toBe("2015-08-02T21:42:04Z");
						expect(user.zip).toBe("012367");

						success();
						done();
					});

				var user = new SalusUser({ // adapted from api call as returned July 2 2015
					"approved": true,
					"city": "cty4",
					"company": "company5",
					"confirmed_at": "2015-08-11T18:15:33",
					"country": "USA-",
					"created_at": "2015-06-11T18:14:42Z",
					"email": "jesse@digitalfoundry.com",
					"firstname": "Jesse-t",
					"lastname": "Shaver-t",
					"phone_country_code": "01235",
					"phone": "457855",
					"state": "state4",
					"street": "street7",
					"updated_at": "2015-08-02T21:42:04Z",
					"zip": "012367"
				});
				user.persist();
			});

		});

		describe("When changing the user password", function () {
			beforeEach(function () {
				SpecHelper.ajax.setup();
			});

			afterEach(function () {
				SpecHelper.ajax.cleanup();
			});

			it("expect password call to fail right away if passwords don't match", function (done) {
				var user = new SalusUser({
					"email": "jesse@digitalfoundry.com",
					"firstname": "Jesse-t",
					"lastname": "Shaver-t"
				});

				user.updatePassword("old", "new 1", "new 2").then(
					function (success) {
						this.fail("changing password to mismatched new password should always fail!");
						done();
					},
					function (fail) {
						expect(fail).toBe("common.validation.passwords.notMatching");
						done();
					}
				);
			});

			it("expect proper change call to server with new and old password", function (done) {
				var handler = {};

				handler.saveProfile = function (proto, url, verb, data, success, fail, raw) {
					expect(proto).toBe(AylaConfig.getURLProtocolForAylaCalls());
					expect(verb).toBe("POST");
					expect(data).toBeTruthy();

					var pData = JSON.parse(data);
					expect(pData).toBeTruthy();
					expect(pData.user).toBeTruthy();
					expect(_.keys(pData).length).toBe(1);

					var user = pData.user;
					expect(user.current_password).toBe("old");
					expect(user.password).toBe("new 1123");

					success();
				};

				spyOn(handler, "saveProfile").and.callThrough();

				SpecHelper.ajax.registerHandler("ajax", AylaConfig.endpoints.userProfile.save, handler.saveProfile);

				var user = new SalusUser({
					"email": "jesse@digitalfoundry.com",
					"firstname": "Jesse-t",
					"lastname": "Shaver-t"
				});

				user.updatePassword("old", "new 1123", "new 1123").then(
					function (success) {
						expect(handler.saveProfile).toHaveBeenCalled();
						done();
					},
					function (fail) {
						this.fail("passwords match, this should succeed");
						done();
					}
				);
			});
		});
	});
});