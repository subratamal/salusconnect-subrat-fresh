"use strict";

define([
	"app",
	"common/util/utilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/dashboard/models/TileOrderCollection",
	"common/constants",
	"consumer/dashboard/views/header/DashboardHeaderLayout.view",
	"consumer/dashboard/views/TileManager.view",
	"slick",
	"jquery.mobile",
	"touch-punch"
], function (App, utils, consumerTemplates, SalusPageMixin, RegisteredRegions, TileOrderCollection, constants) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn, $, _) {
		Views.DashboardPage = Mn.LayoutView.extend({
			template: consumerTemplates["dashboard/dashboard"],
			className: "container",
			regions: {
				header: ".bb-dashboard-header"
			},
			ui: {
				tiles: ".bb-dashboard-tiles",
				innerHeader: ".bb-dashboard-inner-header"
			},
			initialize: function () {
				_.bindAll(this, "_buildTileManagerViews", "_showCurrentTileManager");
				this.manager = this.options.manager;
				this._tileViews = [];
				this._headerViews = [];
				this.listenTo(this.manager, "gatewayInfo:loaded", this.render);
			},

			onBeforeRender: function() {
				if (this.isSlickInit) {
					this.ui.innerHeader.off();

					try {
						this.ui.innerHeader.slick('unload');
						this.ui.innerHeader.slick('unslick');
					} catch (e) {
						//do nothing.
					}
					this.isSlickInit = false;
				}
			},

			onRender: function () {
				// Add the blue dashboard color to the body so it covers 100% of the background
				$("body").addClass("dashboard-background-color");
				this._buildHeader();
			},

			_showCurrentTileManager: function () {

				// if(App.currentTileMover && App.tileMoverModeActive){
				// 	var data = {
				// 		evt: App.currentTileMover.$el,
        //                 view: App.currentTileMover
        //             }
        //             App.tileManagerView._decideTileMoverMode(data);
				// }
				//
				// var timeOut = 0;
				//
				// if(App.flippedTile){
				// 	App.flippedTile.$el.hasClass("grow-container") ? timeOut = 1500 : timeOut = 1000;
				// 	App.flippedTile.closeTile();
				// 	App.flippedTile = null;
				// 	if(App.groupPageView) {
				// 		App.groupPageView.closeModal();
				// 		App.groupPageView = null;
				// 	}
				// } else if(App.groupPageView){
				// 	App.groupPageView.closeModal();
				// 	App.groupPageView = null;
				// 	timeOut = 200;
				// }

				setTimeout(function(){
					var currentTile = this.$(".bb-tile-manager").not(".not-visible"),
						nextTile = this.$('.bb-tile-manager[data-gateway-dsn="' + App.getCurrentGatewayDSN() + '"]');

					currentTile.addClass("not-visible");
					currentTile.find(".bb-arc-group").hide();
					nextTile.find(".bb-arc-group").show();
					nextTile.removeClass("not-visible");

					// swapping the old tile manager view with the new view
					// so the dom is at the top of the container
					currentTile.before(nextTile);

					if (!App.justLogIn) {
						App.salusConnector.trigger("changed:currentGateway");
					}
					App.justLogIn = false;
				}, 0);
			},

			_showCurrentTileManagerWrapper: function() {
					var that = this;

					if(App.currentTileMover && App.tileMoverModeActive){
						var data = {
							evt: App.currentTileMover.$el,
							view: App.currentTileMover
						}
						App.tileManagerView._decideTileMoverMode(data);
					}

					var closeModalPromise = P.resolve([]);
					var flipTilePromise = P.resolve([]);
					var promiseContainer =  P.resolve([]);

					if(App.flippedTile) {
						promiseContainer = flipTilePromise = App.flippedTile.closeTile();
						App.flippedTile = null;
						if(App.groupPageView) {
							promiseContainer = closeModalPromise = App.groupPageView.closeModal(flipTilePromise);
						}
					} else if(App.groupPageView){
							promiseContainer = closeModalPromise = App.groupPageView.closeModal();
					}

					promiseContainer
						.then(function(){
								that._showCurrentTileManager();
						});
			},


			/**
			 * This will not be using the register region mixin
			 * because we need to wait for the view to show on the page instead of just render
			 * for the tiles to calculate their positions correctly
			 */
			onShow: function () {
				var that = this;

				//this.listenTo(this.manager, "change:currentGateway", this._showCurrentTileManager);
				this.listenTo(this.manager, "change:currentGateway", this._showCurrentTileManagerWrapper);
				// We defer the show of the tile manager until we have a collection of devices for tile construction
				// NOTE: Listen to event rather than Promise
				this.listenTo(this.manager, "loaded:dashboard", function () {
					if (!that.manager) {
						return null;
					}

					var user = that.manager.get("sessionUser");

					if (!user) {
						return [];
					}

					// user has already visited the dashboard during the session and has a living tile order
					if (user.get("tileOrderCollection").isLoaded()) {
						that._buildTileManagerViews();

						return true;
					} else if (user.get("tileOrderCollection").isLoading()) {
						return user.get("tileOrderCollection").loadPromise.then(that._buildTileManagerViews);
					}

					return user.get('tileOrderCollection').load().then(that._buildTileManagerViews);
				});
			},
			// author Srikanth N S
			// Filter TileOrder collection (clone the collection)for the gateway to show groups and devices on dashboard

			getGroupDevices: function(groupInfo, callback){
				var device_count = groupInfo.get('device_count');
				var devices = groupInfo.get("devices");
				callback(devices, device_count);
			},

			getDevices:function(groups, callback){
				var that = this, cb = callback, totalGroupDeviceCount = 0,
				groupDevices = [];
				_.each(groups,function(group){
					var groupInfo = App.salusConnector.getGroup(group.get('referenceId'));
					if(groupInfo && groupInfo.get("name").indexOf(constants.tileOrderGroupName) === -1){
						console.info("1");
						that.getGroupDevices(groupInfo, function(devices, device_count){
							totalGroupDeviceCount = totalGroupDeviceCount + device_count;
							groupDevices = groupDevices.concat(devices);
						});
					}
				});

				if(totalGroupDeviceCount == groupDevices.length){
					cb(totalGroupDeviceCount, groupDevices);
				}
			},

			checkForDashBoardHeaderWidth: function() {
				var dashboardHeardeArr= $(".dashboard-header.slick-slide.slick-active.slick-slide.slick-current");
				_.each(dashboardHeardeArr, function(dashboardHeardeEle) {
					var ele = $(dashboardHeardeEle);
					if (ele.width() == 0) {
						var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
						ele.width(width < 1160? width : 1160);
						//console.log("-check width- auto", ele.width());
					}else{
						//console.log("-check width- auto", ele.width());
					}
				});
			},

			filterDevicesAlreadyPresentInGroup:function(currentGatewayDSN,cb){
				var that = this;
				var collection = App.salusConnector.getSessionUser().getTileOrderForGateway(currentGatewayDSN);
				// Get all the group collections and devices
				// and filter out group devices showing on dashboard

				var groups = _.filter(collection.models, function(item){
					return item.get('referenceType') === "group";
				});

				this.getDevices(groups, function(totalGroupDeviceCount, groupDevices){
					var filtered = _.filter(collection.models, function(item){
			            if(item.get('referenceType') === "group") {
			             	//author Srikanth N S, filtering groups which doesn't have any pinned devices
			              	var groupInfo = App.salusConnector.getGroup(item.get('referenceId'));
			              	if(groupInfo && groupInfo.get("name").indexOf(constants.tileOrderGroupName) === -1) {
			                	var onlyPinnedDevices = _.filter(groupInfo.get("devices"), function(deviceId) {
			                    	var deviceModel = App.salusConnector.getDevice(deviceId);
		                        	if (deviceModel.get("isInDashboard")) {
		                            	return deviceId;
		                        	}
			                	});
	                        	if(onlyPinnedDevices.length) {
	                        		groupInfo.set('pinned_device_count',onlyPinnedDevices.length);
                           			groupInfo.set('pinned_devices',onlyPinnedDevices);
	                  				return item;
	                			}
			              	}
			            } else if (groupDevices.indexOf(item.get('referenceId')) === -1 && item.get('referenceType') !== "group") {
			              var deviceModel2 = App.salusConnector.getDevice(item.get('referenceId'));
			              // Remove devices of type ...
			              var devicesToHide = ['it600TRV', 'it600Receiver', 'it600WC', 'it600MINITRV', 'it600Repeater'];
			              if(deviceModel2 &&  deviceModel2.get("isInDashboard") ) {
			              	if(devicesToHide.indexOf(deviceModel2.get('oem_model')) !== -1 ){
			              		deviceModel2.set("isInDashboard", null);
			              	}else{
			              		return item;
			              	}
			              } else if(!deviceModel2) {
			                return item;
			              }
			            }
		          	});
		          	var currentGatewayDSNColle = new TileOrderCollection(filtered, currentGatewayDSN);
					that.listenTo(currentGatewayDSNColle, "tileOrder:modification",  that._onModification);
					that.checkForDashBoardHeaderWidth();
					cb(currentGatewayDSNColle);
				});
			},

			_onModification: function (argument) {
				// actually save

				App.salusConnector.getSessionUser().saveTileOrder(this.getSaveObj(argument));

				// propagate up, used by isInDashboard mixin
				//this.trigger("tileOrder:modification", argument);
			},

			getSaveObj: function (devices) {
				var grpDeviceJSON = [];
				var gWDSN = devices.gatewayDsn;

				devices.map(function (tileModel) {
					if(tileModel.get('referenceType') == "group") {
						var groupInfo = App.salusConnector.getGroup(tileModel.get('referenceId'));
						if(groupInfo){
							var grpDevices = groupInfo.get("devices");

							_.each(grpDevices, function(deviceId) {
								var deviceModel = App.salusConnector.getDevice(deviceId);
				                    if (deviceModel.get("isInDashboard")) {
				                        grpDeviceJSON.push({'referenceType': 'device', 'referenceId': deviceModel.get('key')});
				                    }
							});
						}
					}
					grpDeviceJSON.push(tileModel.pick('referenceType', 'referenceId'));

				});

				var returnObject = {};
				Object.defineProperty(returnObject, gWDSN, {
  					enumerable : true,
  					value: grpDeviceJSON
				});

				return returnObject;
			},

			_buildTileManagerViews: function () {
				var that = this, tileView,
						currentGatewayDSN = App.getCurrentGatewayDSN();


				this._tileViews.forEach(function (tileView) {
					tileView.destroy();
				});
				this._tileViews = [];

				this.filterDevicesAlreadyPresentInGroup(currentGatewayDSN,function(filteredCollection){

					tileView = new Views.TileManagerView({
						dashboardManager: that.manager,
						collection:filteredCollection ,
						gatewayDSN: currentGatewayDSN,
						shouldBeHidden: false
					}).render();

					that._tileViews.push(tileView);

					if (!_.isString(that.ui.tiles) && that.ui.tiles.is(":visible")) {
						that.ui.tiles.append(tileView.$el);
					}

					tileView.completeOnShow();

				});

				App.salusConnector.getGatewayCollection().each(function (gateway) {
					var dsn = gateway.get("dsn");
					if (dsn !== currentGatewayDSN) {
						that.filterDevicesAlreadyPresentInGroup(dsn, function(filteredCollectionOfNonCurrentdsn){
							tileView = new Views.TileManagerView({
								dashboardManager: that.manager,
								collection: filteredCollectionOfNonCurrentdsn,
								gatewayDSN: dsn,
								shouldBeHidden: true
							}).render();
							that._tileViews.push(tileView);

							if (!that.isDestroyed && that.ui.tiles.is(":visible")) {
								that.ui.tiles.append(tileView.$el);
							}

							tileView.completeOnShow();
						});

					}
				});
	        },

			_buildHeader: function () {
				var that = this;
				var gatewayInfos = this.manager.get("gatewayInfoCollection");

				this._headerViews.forEach(function (headerView) {
					headerView.destroy();
				});
				this._headerViews = [];

				if (gatewayInfos && !gatewayInfos.isEmpty()) {
					gatewayInfos.each(function (gatewayInfo) {
						var headerView = new App.Consumer.Dashboard.Views.DashboardHeaderLayout({
							manager: that.manager,
							gateway: gatewayInfo.get("gateway")
						});

						that._headerViews.push(headerView);
						that.ui.innerHeader.append(headerView.render().$el);
					});
				} else {
					var headerView = new App.Consumer.Dashboard.Views.DashboardHeaderLayout({
						manager: that.manager
					});

					this._headerViews.push(headerView);

					this.ui.innerHeader.append(headerView.render().$el);
				}

				//after init, navigate to the current gateways slide
				this.ui.innerHeader.on("init", function (event, slickTrack) {
					that.isSlickInit = true;
					var currentGateway = App.getCurrentGateway();

					if (currentGateway) {
						var slideNum = that.$('[data-gateway-dsn="' + currentGateway.get("dsn") + '"]').first().data("slick-index");
						slickTrack.slickGoTo(slideNum, true); // false means dont animate the slide change
					}

					that.listenTo(that.manager, "allPhotos:loaded", function () {
						that._fixSlickClones();
					});
				});

				this.ui.innerHeader.slick({
					arrows: false,
					lazyLoad: "progressive",
					infinite: true
				});

				this.ui.innerHeader.on("afterChange", function (event, slick, slide) {
					var $lide = $(event.currentTarget).find('[data-slick-index="' + slide + '"]');
					App.salusConnector.changeCurrentGateway($lide.data("gateway-dsn"));
				});
			},

			_fixSlickClones: function () {
				var that = this;
				this.$(".slick-cloned").each(function (i, clone) {
					var $clone = $(clone),
							dsn = $clone.data("gateway-dsn");
					if (dsn) {
						var backgroundUrl,
								gateway = App.salusConnector.getDevice(dsn);

						// we get it from the manager if we have no gateway
						if (gateway) {
							backgroundUrl = that.manager.getGatewayInfo(gateway).get("backgroundImage") || that.manager.defaultBackground(gateway);
						} else {
							backgroundUrl = that.manager.get("backgroundImage");
						}

						if (backgroundUrl) {
							utils.setBackgroundImage($clone.find(".bb-dashboard-header-main"), backgroundUrl);
						}
					}
				});
			},

			onBeforeDestroy: function () {
				// Remove the blue background color from the body
				$("body").removeClass("dashboard-background-color");
				this.ui.innerHeader.off();
				this.ui.innerHeader.slick('unload');
				this.ui.innerHeader.slick('unslick');

				if (App.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise()) {
					App.salusConnector._aylaConnector.dataRefreshManager.dataRefreshPromise().cancel();
				}
			},

			onDestroy: function () {
				var dc = this.manager.get("deviceCollection");
				if (dc) {
					dc.destroy();
				}

				this.manager.off();

				this._tileViews.forEach(function (tileView) {
					tileView.destroy();
				});

				this._headerViews.forEach(function (headerView) {
					headerView.destroy();
				});
			}
		}).mixin([SalusPageMixin, RegisteredRegions], {
			analyticsSection: "dashboard",
			analyticsPage: "dashboard"
		});
	});

	return App.Consumer.Dashboard.Views.DashboardPage;
});
