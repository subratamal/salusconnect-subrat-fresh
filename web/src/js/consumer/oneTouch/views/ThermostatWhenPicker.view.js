"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusTextBox.view"
], function (App, constants, consumerTemplates, SalusView) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn) {

		// item in a menu
		Views.ThermostatWhenPickerView = Mn.ItemView.extend({
			className: "thermostat-when-picker",
			template: consumerTemplates["oneTouch/thermostatWhenPicker"],
			bindings: {
				".bb-above-row input": "aboveInputString",
				".bb-below-row input": "belowInputString"
			},
			events: {
				"blur input": "_handleTempInputChange"
			},
            ui: {
              aboveError:".bb-above-row .bb-error",
              belowError:".bb-below-row .bb-error"
            },

			initialize: function (options) {
				this.type = options.menu.type;
				this.manager = options.manager;
				this.isCondition = options.menu.menuModel.get("type") === constants.oneTouchMenuTypes.condition;
                
                if(this.isCondition){
                    this.minHeat=0;
                    this.maxHeat=99;
                }else{
                    this.minHeat=5;
                    this.maxHeat=35;
                }

				this.subType = options.menu.menuModel.get("subType");

				if (this.type === "between") {
					this.model = new B.Model({
						labelAboveKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.selectAboveValue",
						labelBelowKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.selectBelowValue",
						aboveInputString: null,
						belowInputString: null
					});
				} else {
					this.model = new B.Model({
						labelAboveKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.enterValue",
						labelBelowKey: "equipment.oneTouch.menus.when.stateOfEquipment.thermostats.enterValue",
						aboveInputString: null,
						belowInputString: null
					});
				}

				this.aboveTextBox =  new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "select-temp-text-box",
					inputPlaceholder: "",
					inputType: "number",
					overlayedUnits: App.translate("common.symbols.celsius"),
					hideErrorTriangle: true
				});

				this.belowTextBox =  new App.Consumer.Views.SalusTextBox({
					wrapperClassName: "select-temp-text-box",
					inputPlaceholder: "",
					inputType: "number",
					overlayedUnits: App.translate("common.symbols.celsius"),
					hideErrorTriangle: true
				});
			},

			onRender: function () {
				if (this.type === "above") {
					this.$(".bb-below-row").detach();
				} else if (this.type === "below") {
					this.$(".bb-above-row").detach();
				} else if (this.type !== "between") {
					this.$(".bb-below-row").detach();
				}

				this.$(".bb-above-row .bb-temperature-input").append(this.aboveTextBox.render().$el);
				this.$(".bb-below-row .bb-temperature-input").append(this.belowTextBox.render().$el);
			},

			onDestroy: function () {
				this.aboveTextBox.destroy();
				this.belowTextBox.destroy();
			},

			_limitValue: function (value) {
				// todo: these might need F / C conversions
				if (value < this.minHeat) {
					return this.minHeat;
				} else if (value > this.maxHeat) {
					return this.maxHeat;
				} else {
					return value;
				}
			},

			_roundByHalfs: function (value) {
				return (Math.round(value * 2) / 2).toFixed(1);
			},

			_roundAndLimitModel: function () {
				var that=this,parsedAbove = parseFloat(this.model.get("aboveInputString")),
						parsedBelow = parseFloat(this.model.get("belowInputString"));

				if (!isNaN(parsedAbove)) {
					parsedAbove = this._limitValue(parsedAbove);
					parsedAbove = this._roundByHalfs(parsedAbove);

					this.model.set("aboveInputString", parsedAbove);
				}

				if (!isNaN(parsedBelow)) {
					parsedBelow = this._limitValue(parsedBelow);
					parsedBelow = this._roundByHalfs(parsedBelow);

					this.model.set("belowInputString", parsedBelow);
				}
                
                if(isNaN(parsedAbove)){
                    this.ui.aboveError.text(App.translate("schedules.schedule.newInterval.temperatureError", {
                                    floor: this.minHeat,
                                    ceiling: this.maxHeat
                                }));
                } else {
                    this.ui.aboveError.text("");
                } 
                
                if(isNaN(parsedBelow)){
                    this.ui.belowError.text(App.translate("schedules.schedule.newInterval.temperatureError", {
                                    floor: this.minHeat,
                                    ceiling: this.maxHeat
                                }));
                } else {
                    this.ui.belowError.text("");
                }
			},

			getRuleData: function () {
				var parsedAbove = parseFloat(this.model.get("aboveInputString")),
						parsedBelow = parseFloat(this.model.get("belowInputString"));
                
                if(isNaN(parsedAbove) && isNaN(parsedBelow)){
					return false;
				}

				if (this.isCondition) {
					return {
						type: "propVal",
						gt: parsedAbove * 100, // converting to x100 style the server expects
						lt: parsedBelow * 100, // converting to x100 style the server expects
						propName: this.manager.currentDevice.getPropertyName("LocalTemperature_x100"),
						device: this.manager.currentDevice,
						EUID: this.manager.currentDevice.getEUID()
					};
				} else {
					return {
						type: "propChange",
						propName: [
							this.manager.currentDevice.getSetterPropNameIfPossible("HeatingSetpoint_x100"),
							this.manager.currentDevice.getSetterPropNameIfPossible("CoolingSetpoint_x100")
						],
						newVal: parsedAbove * 100,
						device: this.manager.currentDevice,
						EUID: this.manager.currentDevice.getEUID()
					};
				}
			},

			_handleTempInputChange: function () {
				this._roundAndLimitModel();
			}
		}).mixin([SalusView]);

	});

	return App.Consumer.OneTouch.Views.DayOfWeekPickerView;
});