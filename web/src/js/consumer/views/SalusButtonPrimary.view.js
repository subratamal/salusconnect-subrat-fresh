"use strict";

define([
	"app",
	"consumer/views/mixins/mixin.salusButton"
], function (App, SalusButtonMixin) {
	App.module("Consumer.Views", function (Views, App, B, Mn) {
		/**
		 * options:
		 * {
		 *	id: "sign-in-btn",
		 *	classes: "",
		 * 	buttonTextKey: "login"
		 *	clickedDelegate: function(event, button){}
		 *	}
		 */
		Views.SalusButtonPrimaryView = Mn.ItemView.extend({
			tagName: "button",
			className: "btn btn-primary",
			attributes: {
				"type": "button"
			},
			template: false,
			onRender: function () {
				this.$el.text(App.translate(this.buttonTextKey));
			},
			showSpinner: function () {
				this.$el.addClass("spinning");
			},
			hideSpinner: function () {
				this.$el.removeClass("spinning");
			}
		}).mixin([SalusButtonMixin], {
			eventNameKey: "buttonTextKey",
			eventName: "click"
		});
	});

	return App.Consumer.Views.SalusButtonPrimaryView;
});
