"use strict";

define([
	"app",
	"common/util/utilities",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, utils, templates, SalusView) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn) {
		Views.ScanLayout = Mn.ItemView.extend({
			template: templates["dashboard/header/scan"],
			className: "dashboard-scan",
			ui: {
				"overlay": ".bb-overlay",
				"progressBar": ".bb-progress-bar",
				"status": ".bb-scan-status",
				"complete": ".bb-scan-complete",
				"deviceStatus": ".bb-current-device",
				"resultsControl": ".bb-scan-results-control"
			},
			events: {
				"click @ui.resultsControl": "handleScanResultsControlClick",
				"click .bb-close": "handleScanCloseClick",
				"click .bb-cancel-scan-btn-yes": "cancelScan",
				"click .bb-cancel-scan-btn-no": "closeScanCancelOverlay"
			},
			initialize: function () {
				this.manager = this.options.manager;
				this.showingResults = false;

				this.listenTo(this.manager, "change:isScanningDevices", this.render);
				this.listenTo(this.manager, "change:currentScanIndex", this.updateProgress);
				this.listenTo(this.manager, "change:backgroundImage", this.setBackgroundImage);
			},
			onRender: function () {
				this.setBackgroundImage();
				this.updateStatus();
				this.updateProgress();
			},

			setBackgroundImage: function () {
				// we need to load the image into the overlay as well to give it the blurred look
				utils.setBackgroundImage(this.ui.overlay, this.manager.get("backgroundImage"));
			},

			updateStatus: function () {
				this.ui.status.toggleClass("hidden", !this.manager.get("isScanningDevices"));
				this.ui.complete.toggleClass("hidden", this.manager.get("isScanningDevices"));
			},

			updateProgress: function () {
				var width = 0,
						deviceCollection = this.manager.get("deviceCollection"),
						currentlyScanningIndex = this.manager.get("currentScanIndex");

				if (deviceCollection && deviceCollection.length > 0) {
					var currentlyScanning = deviceCollection.at(currentlyScanningIndex - 1);
					var deviceName = currentlyScanning.getDisplayName();
					width = currentlyScanningIndex / deviceCollection.length;
					width *= 100; // change to percent

					this.ui.progressBar.css("width", width + "%");
					this.ui.deviceStatus.text(deviceName);
				}
			},

			handleScanResultsControlClick: function () {
				this.ui.resultsControl.toggleClass("eye-close", this.showingResults);
				this.ui.resultsControl.toggleClass("eye-open", !this.showingResults);

				this.showingResults = !this.showingResults;

				this.trigger("click:resultsControl");
			},

			_toggleCloseScanOverlay: function () {
				if (this.$(".bb-overlay-scan-cancel").hasClass("hidden")) {
					this.$(".bb-overlay-scan-cancel").removeClass("hidden");
				} else {
					this.$(".bb-overlay-scan-cancel").addClass("hidden");
				}
			},

			handleScanCloseClick: function () {
				// check if scan is done. if done trigger close "click:close"
				// else pop up cancel screen.
				if (this.manager.get("isScanningDevices")) {
					this._toggleCloseScanOverlay();
				} else {
					this.trigger("click:close");
				}
			},

			closeScanCancelOverlay: function () {
				this._toggleCloseScanOverlay();
			},

			cancelScan: function () {
				this.manager.stopScan();
				this.trigger("click:close");
			},

			onBeforeDestroy: function () {
				this.stopListening(this.manager);
				this.manager = null;
				delete this.manager;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.ScanLayout;
});