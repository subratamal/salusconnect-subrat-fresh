"use strict";

define([
	"app",
	"consumer/myStatus/models/MyStatusModel",
	"common/constants"
], function (App, MyStatusModel, constants) {
	App.module("Consumer.MyStatus.Models", function (Models, App, B) {
		var MyStatusModel = App.Consumer.MyStatus.Models.MyStatusModel;
		var topColors = constants.myStatusTileColors.topColors,
			botColors = constants.myStatusTileColors.botColors;

		Models.newMyStatusIT600Collection =
			new B.Collection([
				new MyStatusModel({
					icon: "away",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "cloud",
					topColor: topColors.brown,
					botColor: botColors.brown
				}),
				new MyStatusModel({
					icon: "moon",
					topColor: topColors.blue,
					botColor: botColors.blue
				}),
				new MyStatusModel({
					icon: "sun",
					topColor: topColors.orange,
					botColor: botColors.orange
				}),
				new MyStatusModel({
					icon: "martini",
					topColor: topColors.blue,
					botColor: botColors.blue
				})
			]);
	});

	return App.Consumer.MyStatus.Models.newMyStatusIT600Collection;
});
