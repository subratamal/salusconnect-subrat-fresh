"use strict";

define([], function () {

    /**
     * constants is a place mainly for enumerable objects
     */
    var constants = {
        noGateway: "--NOGATEWAY--",
        categoryTypes: {
            "CARBONMONOXIDEDETECTORS": "carbonMonoxideDetectors",
            "DOORMONITORS": "doorMonitors",
            "ENERGYMETERS": "energyMeters",
            "GATEWAYS": "gateways",
            "WATERHEATERS": "waterHeaters",
            "THERMOSTATS": "thermostats",
            "TRVS": "trvs",
            "SMARTPLUGS": "smartPlugs",
            "SMOKEDETECTORS": "smokeDetectors",
            "WINDOWMONITORS": "windowMonitors",
            "WIRELESSUNCONTROLLABLEDEVICES": "wirelessUncontrollableDevices"
        },
        modelTypes: {
            CARBONMONOXIDEDETECTOR: "carbonMonoxideDetector",
            COORDINATORDEVICE: "coordinatorDevice",
            DOORMONITOR: "doorMonitor",
            ENERGYMETER: "energyMeter",
            GATEWAY: "gateway",
            IT600BOILERRECEIVER: "iT600BoilerReceiver",
            WATERHEATER: "waterHeater",
            THERMOSTAT: "thermostat",
            IT600TRV: "iT600Trv",
            IT600WIRINGCENTER: "iT600WiringCenter",
            IT600THERMOSTAT: "iT600Thermostat",
            MINISMARTPLUG: "miniSmartPlug",
            SMARTPLUG: "smartPlug",
            SMOKEDETECTOR: "smokeDetector",
            WINDOWMONITOR: "windowMonitor",
            GATEWAYNODE: "gatewayNode",
            GENERIC: "generic"
        },
        thermostatModeTypes: {
            OFF: 0,
            AUTO: 1,
            COOL: 3,
            HEAT: 4,
            EMERGENCYHEATING: 5
        },
        runningModeTypes: {
            OFF: 0,
            COOL: 3,
            HEAT: 4
        },
        fanModeTypes: {
            ON: 4,
            AUTO: 5
        },
        waterHeaterHoldTypes: {
            SCHEDULE: 0,
            OFF: 7,
            BOOST: 8,
            CONTINUOUS: 9
        },
        it600HoldTypes: {
            FOLLOW: 0,
            TEMPHOLD: 1,
            PERMHOLD: 2,
            OFF: 7
        },
        lockKey:{
            LOCK: 1,
            UNLOCK: 0
        },
        thermostatHoldDuration: 65535,
        tileGradients: {
            TILE_GREEN_GRADIENT_TOP: "#7cc110",
            TILE_GREEN_GRADIENT_BOTTOM: "#58a708",
            TILE_RED_GRADIENT_TOP: "#f44336",
            TILE_RED_GRADIENT_BOTTOM: "#eb1c14"
        },
        sceneGradients: [
            {
                TOP: "#7CC110", // home
                BOTTOM: "#58A708"
            },
            {
                TOP: "#0399E4", // away
                BOTTOM: "#027DD9"
            },
            {
                TOP: "#00C1DD", // vacation
                BOTTOM: "#00ACD0"
            }
        ],
        myStatusTileColors: {
            topColors: {
                turq: "#1cddad",
                blue: "#0097a7",
                orange: "#eab925",
                brown: "#bbc532",
                green: "#00df72"
            },
            botColors: {
                turq: "#04ad92",
                blue: "#00838f",
                orange: "#ffa000",
                brown: "#a4a929",
                green: "#00bc4e"
            }
        },
        errorTypes: {
            "error404": "404",
            "REQUEST": "request"
        },
        daysOfWeekMap: [
            "sunday",
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday"
        ],
        daysOfWeek: {
            "SUNDAY": 1,
            "MONDAY": 2,
            "TUESDAY": 3,
            "WEDNESDAY": 4,
            "THURSDAY": 5,
            "FRIDAY": 6,
            "SATURDAY": 7
        },
        scheduleDaysTypes: {
            "WORKWEEK": "workWeek",
            "WEEKEND": "weekend",
            "FULLWEEK": "fullWeek"
        },
        scheduleConfigurationTypes: {
            "WORKWEEK": "workWeek",
            "FULLWEEK": "fullWeek",
            "DAILY": "daily"
        },
        myStatusIconPaths: {
            a: "/images/icons/myStatus/A.svg",
            airplane: "/images/icons/myStatus/Airplane.svg",
            away: "/images/icons/myStatus/Away.svg",
            balloons: "/images/icons/myStatus/Balloons.svg",
            candles: "/images/icons/myStatus/Candles.svg",
            eye: "/images/icons/myStatus/Eye.svg",
            exclamation: "/images/icons/myStatus/Exclamation.svg",
            cone: "/images/icons/myStatus/Cone.svg",
            cloud: "/images/icons/myStatus/Cloud.svg",
            family: "/images/icons/myStatus/Family.svg",
            flame: "/images/icons/myStatus/Flame.svg",
            gift: "/images/icons/myStatus/Gift.svg",
            house_snowflake: "/images/icons/myStatus/House_Snowflake.svg",
            house_sun: "/images/icons/myStatus/House_Sun.svg",
            house: "/images/icons/myStatus/House.svg",
            keys: "/images/icons/myStatus/Keys.svg",
            leaf: "/images/icons/myStatus/Leaf.svg",
            man_hat: "/images/icons/myStatus/Man_Hat.svg",
            martini: "/images/icons/myStatus/Martini.svg",
            moon: "/images/icons/myStatus/Moon.svg",
            snowflake: "/images/icons/myStatus/Snowflake.svg",
            suitcase: "/images/icons/myStatus/Suitcase.svg",
            sun_cloud: "/images/icons/myStatus/Sun_Cloud.svg",
            sun: "/images/icons/myStatus/Sun.svg",
            umbrella: "/images/icons/myStatus/Umbrella.svg"
        },
        schedulesIconPaths: {
            away: "/images/icons/schedules/Away.svg",
            create_new: "/images/icons/schedules/Create_own.svg",
            home: "/images/icons/schedules/Home.svg",
            working: "/images/icons/schedules/Working_wk.svg"
        },
        getStatusIconBackgroundColor: function (icon) {
            switch (icon) {
                case "a":
                    return {
                        topColor: this.myStatusTileColors.topColors.green,
                        botColor: this.myStatusTileColors.botColors.green
                    };
                case "airplane":
                    return {
                        topColor: this.myStatusTileColors.topColors.turq,
                        botColor: this.myStatusTileColors.botColors.turq
                    };
                case "away":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "balloons":
                    return {
                        topColor: this.myStatusTileColors.topColors.green,
                        botColor: this.myStatusTileColors.botColors.green
                    };
                case "candles":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "eye":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "exclamation":
                    return {
                        topColor: this.myStatusTileColors.topColors.green,
                        botColor: this.myStatusTileColors.botColors.green
                    };
                case "cone":
                    return {
                        topColor: this.myStatusTileColors.topColors.orange,
                        botColor: this.myStatusTileColors.botColors.orange
                    };
                case "cloud":
                    return {
                        topColor: this.myStatusTileColors.topColors.brown,
                        botColor: this.myStatusTileColors.botColors.brown
                    };
                case "family":
                    return {
                        topColor: this.myStatusTileColors.topColors.brown,
                        botColor: this.myStatusTileColors.botColors.brown
                    };
                case "flame":
                    return {
                        topColor: this.myStatusTileColors.topColors.orange,
                        botColor: this.myStatusTileColors.botColors.orange
                    };
                case "gift":
                    return {
                        topColor: this.myStatusTileColors.topColors.green,
                        botColor: this.myStatusTileColors.botColors.green
                    };
                case "house_snowflake":
                    return {
                        topColor: this.myStatusTileColors.topColors.turq,
                        botColor: this.myStatusTileColors.botColors.turq
                    };
                case "house_sun":
                    return {
                        topColor: this.myStatusTileColors.topColors.orange,
                        botColor: this.myStatusTileColors.botColors.orange
                    };
                case "house":
                    return {
                        topColor: this.myStatusTileColors.topColors.brown,
                        botColor: this.myStatusTileColors.botColors.brown
                    };
                case "keys":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "leaf":
                    return {
                        topColor: this.myStatusTileColors.topColors.brown,
                        botColor: this.myStatusTileColors.botColors.brown
                    };
                case "man_hat":
                    return {
                        topColor: this.myStatusTileColors.topColors.brown,
                        botColor: this.myStatusTileColors.botColors.brown
                    };
                case "martini":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "moon":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "snowflake":
                    return {
                        topColor: this.myStatusTileColors.topColors.turq,
                        botColor: this.myStatusTileColors.botColors.turq
                    };
                case "suitcase":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "sun_cloud":
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
                case "sun":
                    return {
                        topColor: this.myStatusTileColors.topColors.orange,
                        botColor: this.myStatusTileColors.botColors.orange
                    };
                case "umbrella":
                    return {
                        topColor: this.myStatusTileColors.topColors.turq,
                        botColor: this.myStatusTileColors.botColors.turq
                    };
                default:
                    return {
                        topColor: this.myStatusTileColors.topColors.blue,
                        botColor: this.myStatusTileColors.botColors.blue
                    };
            }
        },
        widgetTypes: {
            STATUS: "status",
            INFORMATION: "info",
            HISTORY: "history",
            USAGE: "usage",
            TRV: "trv",
            SCHEDULE: "schedule"
        },
        equipmentLists: {
            category: "category",
            group: "group"
        },
		tileOrderGroupName: "__tile_order__",
		tileOrderGroupProperties: [
			"ep_1:sIASZS:ErrorIASZSAlarmed1",
			"ep_1:sZDO:LeaveNetwork",
			"ep_9:sOnOffS:OnOff",
			"ep_9:sIT600HW:HoldType",
			"ep_9:sIT600HW:RunningMode",
			"ep_9:sIT600HW:SystemMode",
			"ep_9:sIT600TH:HoldType",
			"ep_9:sIT600TH:RunningMode",
			"ep_9:sIT600TH:SystemMode",
			"ep_9:sIT600TH:LocalTemperature_x100",
			"ep_9:sTherS:RunningMode",
			"ep_9:sTherS:SystemMode",
			"ep_9:sTherS:LocalTemperature_x100",
			"ep_9:sFanS:FanMode",
			"ep_9:sZDO:LeaveNetwork"
		],
        oneTouchMenuTypes: {
            condition: "condition",
            action: "action",
            delayedAction: "delayedAction"
        },
        // TODO: these mappings need to be updated along with the device collection mappings!
        _deviceTypeMap: {
            "sau2ag1": "equipment.provisioning.gateway",
            "SAU2AG1-ZC": "equipment.provisioning.coordinator",
            "SAU2AG1-GW": "equipment.provisioning.gatewayNode",
            "it600ThermHW_AC": "equipment.provisioning.it600Thermostat",
            "it600ThermHW-AC": "equipment.provisioning.it600Thermostat",
            "it600ThermHW": "equipment.provisioning.it600Thermostat",
			"it600HW-AC": "equipment.provisioning.it600Thermostat",
            "it600HW": "equipment.provisioning.it600Thermostat",
            "it600WC": "equipment.provisioning.wiringCenter",
            "it600Receiver": "equipment.provisioning.boilerReceiver",
            "it600MINITRV": "equipment.provisioning.trv",
            "it600TRV": "equipment.provisioning.trv",
            "ECM600": "equipment.provisioning.energyMeter",
            "SAL2EM1": "equipment.provisioning.energyMeter",
            "SX885ZB": "equipment.provisioning.smartPlug",
            "SPE600": "equipment.provisioning.smartPlug",
            "SP600": "equipment.provisioning.smartPlug",
            "SAL2": "equipment.provisioning.smartPlug",
            "ST880ZB": "equipment.provisioning.optimaThermostat",
            "ST898ZB": "equipment.provisioning.optimaThermostat",
            "iT530TR": "equipment.provisioning.it530Thermostat",
            //"iT330TX": "equipment.provisioning.device",
            //"iT530TX": "equipment.provisioning.device",
            //"iT530RX": "equipment.provisioning.device",
            "CO-8ZBS": "equipment.provisioning.coMonitor",
            "SD-8ZBS-EL": "equipment.provisioning.smokeAlarm",
            "SD-8SCZBS": "equipment.provisioning.smokeAlarm",
            "SS881ZB": "equipment.provisioning.doorSensor",
            "SS882ZB": "equipment.provisioning.windowSensor",
            "OS600": "equipment.provisioning.windowSensor"
        },
        /**
         * map some equipment!
         * translated
         * @param provisionModel
         */
        deviceTypeMapping: function (provisionModel) {
            return this._deviceTypeMap[provisionModel.get("oem_model")] || "equipment.provisioning.device";
        },
        getProvisioningIconClass: function (provisionModel) {
            var oem = provisionModel.get("oem_model"),
                    cssClass = "";

            // todo: we could improve this further since we do have an oem_model property... for now just using some generic ones like 'sensor-image'
            switch (oem) {
                case "sau2ag1":
                    cssClass = "gateway-image";
                    break;
                case "SAU2AG1-ZC":
                case "it600WC":
                case "SAU2AG1-GW":
                    cssClass = "wud-image";
                    break;
                case "it600ThermHW-AC":
                case "it600ThermHW_AC":
                case "it600ThermHW":
				case "it600HW-AC":
				case "it600HW":
                case "ST880ZB":
                case "iT530TR":
                case "ST898ZB":
                    cssClass = "thermostat-image";
                    break;
                case "CO-8ZBS":
                case "SD-8ZBS-EL":
                case "SD-8SCZBS":
                    cssClass = "sensor-image";
                    break;
                case "SS881ZB":
                    cssClass = "door-image";
                    break;  
                case "SS882ZB":
                case "OS600":
                    cssClass = "window-image";
                    break;   
                case "SAL2":
                case "SPE600":
                case "SP600":
                case "SX885ZB":
                    cssClass = "smart-plug-image";
                    break;
                case "SAL2EM1":
                case "ECM600":
                    cssClass = "energy-meter-image";
                    break;
                    // case "iT330TX":
                    // case "iT530TX":
                    // case "iT530RX":
                case "it600MINITRV":
                case "it600TRV":
                    cssClass = "trv-image";
                    break;
                case "it600Receiver":
                    cssClass = "default-image";
                    break;
                default:
                    cssClass = "default-image";
                    break;
            }

            return cssClass;
        },
        daysOfWeekAbbr: [
            "common.daysOfWeek.abbreviations.sunday",
            "common.daysOfWeek.abbreviations.monday",
            "common.daysOfWeek.abbreviations.tuesday",
            "common.daysOfWeek.abbreviations.wednesday",
            "common.daysOfWeek.abbreviations.thursday",
            "common.daysOfWeek.abbreviations.friday",
            "common.daysOfWeek.abbreviations.saturday"
        ],
        countryPhoneCodes: {
            "at": "43",     //Austria
			"be": "32",     //Belgium
			"cz": "420",    //Czech Republic
			"dk": "45",     //Denmark
			"fi": "358",    //Finland
			"fr": "33",     //France
			"de": "49",     //Germany
			"is": "353",    //Ireland
			"it": "39",     //Italy
			"nl": "31",     //Netherlands
			"no": "47",     //Norway
			"pl": "48",     //Poland
			"ro": "40",     //Romania
			"ru": "7",      //Russia
			"es": "34",     //Spain
			"sv": "46",     //Sweden
			"ch": "41",     //Switzerland
			"ua": "380",    //Ukraine
			"uk": "44",     //United Kingdom
			"us": "1"       //United States
        },
        
        thenDoLaterSubTypes: {
            none: "none",
            undo: "undo",
            equipmentChange: "equipmentChange"
        },
        scheduleTypes: {
            thermostats: {
                singleControl: 0,
                individualControl: 1
            }
        },
        smartPlugOnOff: {
            OFF: 0,
            ON: 1
        },
        ruleGatewayNodePseudoEUID: "0000000000000000",
        weekLength: 7,
        spinnerTextKeys: {
            adding: "common.loadingMessages.adding",
            connecting: "common.loadingMessages.connecting",
            loading: "common.loadingMessages.loading",
            processing: "common.loadingMessages.processing",
            saving: "common.loadingMessages.saving",
            scanning: "common.loadingMessages.scanning",
            starting: "common.loadingMessages.starting"
        },
        // Dev_SBO-606 start
        // one touch name中开头是这个字符的都要隐藏
        hideOneTouchKey: "_P65FD8T6S",
        
        // To the default value of the value of the schedule at the same key
        scheduleActionPropertyKeys: {
            heat: "Heating",
            cool: "Cooling"
        },
        
        // 当用户点击default schedule时的默认的schedule
        defaultSchedule: {
            //Working Week
            "workingWeek": {
                "monToFriday": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "satToSun": {
                    "thermostat": [
                        {"time": "08:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1900","Cooling":"1900"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                }
            },
            //Home most of the time
            "homeMostOfTheTime": {
                "monToSun": {
                    "thermostat": [
                        {"time": "08:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1900","Cooling":"1900"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                }
            },
            //Daily
            "daily": {
                "mo": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "tu": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "we": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "th": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "fr": {
                    "thermostat": [
                        {"time": "07:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1700","Cooling":"1700"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "sa": {
                    "thermostat": [
                        {"time": "08:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1900","Cooling":"1900"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
                "su": {
                    "thermostat": [
                        {"time": "08:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "09:00:00", "value": {"Heating":"1900","Cooling":"1900"}},
                        {"time": "17:00:00", "value": {"Heating":"2100","Cooling":"2100"}},
                        {"time": "23:00:00", "value": {"Heating":"1700","Cooling":"1700"}}
                    ],
                    "hotWater": [
                        {"time": "06:00:00", "value": 1},
                        {"time": "09:00:00", "value": 0},
                        {"time": "18:00:00", "value": 1},
                        {"time": "20:00:00", "value": 0}
                    ]
                },
            }

        }
    };

    return constants;
});