"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusButtonPrimary",
	"consumer/login/models/ChangePasswordWithTokenWell.model",
	"consumer/views/SalusTextBox.view",
	"consumer/login/views/ResetPasswordModal.view",
    "consumer/login/views/ChangePasswordConfirmationModal.view",
], function (App, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView, SalusPrimaryButtonModel, ChangePasswordWithTokenModel, SalusTextBox) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.ChangePasswordWithTokenWell = Mn.ItemView.extend({
			id: "bb-change-password-with-token-well",
			className: "change-password-with-token-well",
			template: consumerTemplates["login/changePasswordWithTokenWell"],
			ui: {
				"bbTokenPlaceholder": ".bb-token-input-el-placeholder",
				"bbFirstPasswordPlaceholder": ".bb-first-password-input-el-placeholder",
				"bbSecondPasswordPlaceholder": ".bb-second-password-input-el-placeholder",
				"bbSaveNewPasswordBtnContainer": ".bb-save-new-password-btn-container"
			},
			events: {
				"keydown": "handleEnterButtonPress"
			},
			initialize: function () {
				_.bindAll(this, "triggerSaveNewPassword");

				this.tokenBox = new SalusTextBox({
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 password-token-input",
					inputPlaceholder: App.translate("login.changePasswordWithToken.resetPasswordTokenLabel"),
					inputType: "text",
					inputClass: "bb-secure-token",
					iconPath: App.rootPath("/images/icons/icon_key_blue.svg"),
					iconAlt: "Enter token",
					isRequired: true
				});

				this.firstPasswordBox = new SalusTextBox({
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 new-password-input",
					inputPlaceholder: App.translate("login.changePasswordWithToken.newPasswordLabel"),
					inputType: "password",
					inputClass: "bb-new-password",
					iconPath: App.rootPath("/images/icons/icon_password_blue.svg"),
					iconAlt: "Choose a password",
					isRequired: true
				});

				this.secondPasswordBox = new SalusTextBox({
					wrapperClassName: "col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 confirm-password-input",
					inputPlaceholder: App.translate("login.changePasswordWithToken.confirmPasswordLabel"),
					inputType: "password",
					inputClass: "bb-confirm-password",
					iconPath: App.rootPath("/images/icons/icon_password_blue.svg"),
					iconAlt: "Confirm password",
					isRequired: true
				});

				// TODO move this custom button style that is used here AND in the forgot password button to the global SalusButton styling
				this.saveNewPasswordButton = new SalusPrimaryButtonView({
					classes: "save-new-password-btn",
					buttonTextKey: "login.changePasswordWithToken.button",
					clickedDelegate: this.triggerSaveNewPassword
				});

				// Setup event bindings to update the token and password on the model when the input values change
				this.changePasswordWithTokenModelBindings = {
					'.bb-new-password': 'newPassword',
					'.bb-confirm-password': 'confirmPassword',
					'.bb-secure-token': 'secureToken'
				};

				this.changePasswordWithTokenModel = new ChangePasswordWithTokenModel({
					'secureToken': this.setTokenValue()
				});

				this.setupEventListeners();
			},
			onRender: function () {
				this.ui.bbTokenPlaceholder.replaceWith(this.tokenBox.render().$el);
				this.ui.bbFirstPasswordPlaceholder.replaceWith(this.firstPasswordBox.render().$el);
				this.ui.bbSecondPasswordPlaceholder.replaceWith(this.secondPasswordBox.render().$el);
				this.ui.bbSaveNewPasswordBtnContainer.append(this.saveNewPasswordButton.render().$el);

				this.stickit(this.changePasswordWithTokenModel, this.changePasswordWithTokenModelBindings);

				return this;
			},
			onDestroy: function () {
				this.tokenBox.destroy();
				this.firstPasswordBox.destroy();
				this.secondPasswordBox.destroy();
				this.saveNewPasswordButton.destroy();
			},
			setupEventListeners: function () {
				// Event listener to trigger the save password function when called from the button view
				this.listenTo(this.saveNewPasswordButton, "triggerSaveNewPassword", this.triggerSaveNewPassword);
			},
			/**
			 * Grab the secure token value from the initial model and place it in the view
			 * @returns String result if param exists, otherwise return null
			 */
			setTokenValue: function () {
				var result = null, params = this.model.get("params");

				if (params && params.secureToken) {
					result = params.secureToken;
				}

				return result;
			},
			/**
			 * Trigger the form submit when the enter key is pressed
			 */
			handleEnterButtonPress: function (event) {
				if (event.which && event.which === 13) {
					this.triggerSaveNewPassword();
				}
			},
			triggerSaveNewPassword: function () {
				this.clearInputErrors();

				var salusConnector = App.salusConnector,
					firstPassword = this.changePasswordWithTokenModel.get("newPassword"),
					secondPassword = this.changePasswordWithTokenModel.get("confirmPassword"),
					token = this.changePasswordWithTokenModel.get("secureToken"),
					that = this;

				// Local validations
				if (!token) {
					this.tokenBox.registerError(App.translate("common.validation.ayla.cantbeblank"));
					return;
				}

				if (token.match(/\s+/)) { // token contains one or more white space
					this.tokenBox.registerError(App.translate("common.validation.ayla.tokeninvalid"));
					return;
				}

				if (!firstPassword) {
					this.firstPasswordBox.registerError(App.translate("common.validation.ayla.cantbeblank"));
					return;
				}

				if (!secondPassword) {
					this.secondPasswordBox.registerError(App.translate("common.validation.ayla.cantbeblank"));
					return;
				}

				if (firstPassword !== secondPassword) {
					this.firstPasswordBox.registerError(App.translate("common.validation.passwords.notMatching"));
					this.secondPasswordBox.registerError(App.translate("common.validation.passwords.notMatching"));
					return;
				}

				if (!secondPassword.match(/^(?=.*[a-z])(?=.*[A-Z])((?=.*\d)|(?=.*[-+_!@#$%^&*.,?)(=<>`"';:~|/\\])).+$/)) {
					this.secondPasswordBox.registerError(App.translate("common.validation.passwords.tooSimple"));
					return;
				} 

				salusConnector.setPasswordWithToken(firstPassword, secondPassword, token).then(function () {
                    App.modalRegion.show(new App.Consumer.Views.SalusModalView({
							contentView: new Views.ChangePasswordConfirmationModalView()
					}));
                    App.showModal();

                   
					
				}).catch(function (e) {
					if (e.status === 422) {

						App.modalRegion.show(new App.Consumer.Views.SalusModalView({
							contentView: new Views.ResetPasswordModalView()
						}));

						App.showModal();

						if (e.responseText.includes("is invalid")) {
							that.tokenBox.registerError(App.translate("common.validation.ayla.tokeninvalid"));
							return;
						}
					}
				});
			},
			clearInputErrors: function () {
				this.tokenBox.clearError();
				this.firstPasswordBox.clearError();
				this.secondPasswordBox.clearError();
			}

		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.ChangePasswordWithTokenWell;
});
