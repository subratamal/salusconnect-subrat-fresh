"use strict";

define([
	"app",
	"jquery",
	"underscore",
	"backbone"
], function (App, $, _, B) {

	App.module("Consumer.Models", function (Models) {
		Models.CheckboxViewModel = B.Model.extend({
			defaults: {
				isChecked: false,
				disabled: false,
				name: "",
				value: "",
				labelTextKey: "",
				checkColor: "",
				nonKeyLabel: null,
				id: "",
				classes: ""
			}
		});
	});

	return App.Consumer.Models.CheckboxViewModel;
});
