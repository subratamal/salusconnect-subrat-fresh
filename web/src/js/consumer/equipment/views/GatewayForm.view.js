"use strict";

define([
	"app",
	"bluebird",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/consumer.views",
	"consumer/views/mixins/mixins",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusDropdownViewModel",
	"common/model/ayla/deviceTypes/GatewayAylaDevice"
], function (App, P, config, consumerTemplates, ConsumerViews, ConsumerMixins) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.GatewayForm = Mn.LayoutView.extend({
			template: consumerTemplates["equipment/gateway"],
			className: "gateway-edit",
			regions: {
				"gatewayDsn": ".bb-gateway-dsn",
				"refreshButton": ".bb-button-refresh",
				"gatewayAddress": ".bb-address",
				"gatewayCity": ".bb-city",
                "gatewayTimeZone": ".bb-timezone",
				"gatewayState": ".bb-state",
				"gatewayZip": ".bb-zip",
				"gatewayCountry": ".bb-country",
				"deviceName": ".bb-device-name",
				"saveButton": ".bb-button-save",
				"cancelButton": ".bb-button-cancel"
			},
			ui: {
				"addressContainer": ".bb-gateway-address-container",
				"gatewayStateWrapper": ".bb-state-wrapper",
                "gatewayTipsHeader": ".new-user-section-header"
			},
			events: {
				"click .bb-button-save": "handleSubmitClicked"
			},

			initialize: function (options) {
				var that = this,
					address = this.model.get("address");

				_.bindAll(this, "handleCancelClick", "handleRefreshClick", "_updateAddress");
                
                this.devices = App.salusConnector.getDevicesOnGateway(this.model);
                this.gatewayDevices = [];
             
                _.each(this.devices.models,function(device){
                    if(device.get("oem_model") === "SAU2AG1-GW"){
                        that.gatewayDevices.push(device);
                    }
                });
                
                var promise = P.all(this.gatewayDevices.map(function (device) {
                   return device.getDeviceProperties();
                }));
                promise.then(function () {
                    var gatewayNode = that.model.getGatewayNode();
                    that.timeZone = gatewayNode.get("TimeZone") ? gatewayNode.get("TimeZone").getProperty() : null;
                    // Bug_CASBW-28
                    if(that.timeZone && that.timeZone.indexOf("timezone") < 0) {
                        var arr = that.timeZone.split("/");
                        that.model.set("time_zone", arr ? arr[arr.length-1] : that.timeZone);
                    }
                });


				if (address) {
					this.listenTo(address, "fetch:success", function () {
						that.model.saveState();
					});
				}
                
                this.mode = options.mode;

				if (this.mode === "edit" && !this.model.get("alias")) {
					this.listenToOnce(this.model, 'change:alias', function () {
						that.model.saveState();
					});
				}

				this.listenTo(this.model, 'change:dsn', this.onDsnChange);
                this.listenTo(address, "change:country", this.onCountryChange);
                this.listenTo(this.model, "change:time_zone", this.onTimeZoneChange);

				this.countriesCollection = new App.Consumer.Models.DropdownItemViewCollection();
                this.timeZonesCollection = new App.Consumer.Models.DropdownItemViewCollection();
                
				this._fillDropdownCollection(config.countries, config.countryPrefix, this.countriesCollection);
                
                if(address && address.get("country") === "us") {
                    this._fillDropdownCollection(config.timeZones_us, config.timeZoneConvertedPrefix_us, this.timeZonesCollection);
                } else {
                    this._fillDropdownCollection(config.timeZones, config.timeZoneConvertedPrefix, this.timeZonesCollection);
                }

				this.gatewayDsnField = this.registerRegion("gatewayDsn", new ConsumerViews.FormTextInput({
					labelText: "equipment.gateway.deviceSerialNumber",
					required: false,
					model: this.model,
					modelProperty: "dsn",
					readonly: true
				}));

				this.refreshButtonView = this.registerRegion("refreshButton", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "equipment.gateway.searchButton",
					className: "btn btn-primary min-width-btn",
					clickedDelegate: this.handleRefreshClick
				}));

				this.gatewayAddressField = this.registerRegion("gatewayAddress", new ConsumerViews.FormTextInput({
					labelText: "equipment.gateway.address.addressLabel",
					required: true,
					model: address,
					modelProperty: "street",
					maxLength: 255
				}));

				this.gatewayCityField = this.registerRegion("gatewayCity", new ConsumerViews.FormTextInput({
					labelText: "equipment.gateway.address.cityLabel",
					validationMethod: function () {
						if (App.getIsEU()) {
							return {valid: true};
						} else {
							return App.validate.words(this.getValue());
						}
					},
					required: true,
					model: address,
					modelProperty: "city",
					maxLength: 255
				}));

				if (App.getIsEU()) {
					// No states for EU, so set it to space for validation purposes
					address.set("state", " ");
				} else {
					this.gatewayStateField = this.registerRegion("gatewayState", new ConsumerViews.FormTextInput({
						labelText: "equipment.gateway.address.stateLabel",
						validationMethod: function () {
							return App.validate.words(this.getValue());
						},
						required: true,
						model: address,
						modelProperty: "state"
					}));
				}

				this.gatewayZipField = this.registerRegion("gatewayZip", new ConsumerViews.FormTextInput({
					labelText: App.getIsEU() ? "equipment.gateway.address.postcodeLabel" : "equipment.gateway.address.zipLabel",
					validationMethod: function () {
						if (App.getIsEU()) {
							return {valid: true};
						} else {
							return App.validate.zip(this.getValue());
						}
					},
					required: true,
					model: address,
					modelProperty: "zip"
				}));

				this.gatewayCountryField = this.registerRegion("gatewayCountry", new ConsumerViews.SalusDropdownWithValidation({
					collection: this.countriesCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "gatewayCountrySelect",
						dropdownLabelKey: "setupWizard.newUserForm.formFields.countrySelect",
                        labelTextKey: "setupWizard.newUserForm.formFields.countrySelect"
					}),
					required: true,
					boundAttribute: "country",
					model: address
				}));
                
                this.gatewayTimeZoneField = this.registerRegion("gatewayTimeZone", new ConsumerViews.SalusDropdownWithValidation({
                    collection: this.timeZonesCollection,
                    viewModel: new App.Consumer.Models.DropdownViewModel({
                        id: "gatewayTimeZoneSelect",
                        dropdownLabelKey: "setupWizard.newUserForm.formFields.timeZoneSelect",
                        labelTextKey: "setupWizard.newUserForm.formFields.timeZoneSelect"
                    }),
                    required: true,
                    boundAttribute: "time_zone",
                    model: this.model
                }));

				this.gatewayAliasField = this.registerRegion("deviceName", new ConsumerViews.FormTextInput({
					labelText: "equipment.gateway.deviceInputLabel",
					model: this.model,
					required: true,
					modelProperty: "alias",
					value: this.model.getDisplayName()
				}));

				this.registerRegion("cancelButton", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					className: "btn btn-default",
					classes: "width100",
					clickedDelegate: this.handleCancelClick
				}));

				this.saveButtonView = this.registerRegion("saveButton", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: this.mode === "edit" ? "common.labels.save" : "equipment.gateway.submitButton",
					classes: this.mode === "edit" ? "width100 disabled" : "width100"
				}));

				if (App.getIsEU()) {
					this.validationNeeded.push(this.gatewayDsnField, this.gatewayAddressField, this.gatewayCityField,
							this.gatewayZipField, this.gatewayCountryField, this.gatewayTimeZoneField, this.gatewayAliasField);
				} else {
					this.validationNeeded.push(this.gatewayDsnField, this.gatewayAddressField, this.gatewayCityField,
							this.gatewayStateField, this.gatewayZipField, this.gatewayCountryField, this.gatewayTimeZoneField, this.gatewayAliasField);
				}

				this.attachValidationListener();

				this.startExpanded = options.startExpanded || false;
			},

			onDsnChange: function () {
				if (this.model.get("dsn")) {
					this.ui.addressContainer.show();
				}
			},
            
            onCountryChange: function () {
                var arr = this.timeZone ? this.timeZone.split("/") : null;
                var time_zone = arr ? arr[arr.length-1] : this.timeZone;
                var address, listView;
                address = this.model.get("address");
                this.saveButton.currentView.enable();
                this.timeZonesCollection = new App.Consumer.Models.DropdownItemViewCollection();
                if(address && address.get("country") === "us") {
                    // Bug_CASBW-28
                    this.model.set("time_zone", this.model.get("currentTimeZone_us") ? this.model.get("currentTimeZone_us") : "Los_Angeles");
                    this._fillDropdownCollection(config.timeZones_us, config.timeZoneConvertedPrefix_us, this.timeZonesCollection);
                } else {
                    // Bug_CASBW-28
                    this.model.set("time_zone", this.model.get("currentTimeZone") ? this.model.get("currentTimeZone") : (time_zone ? time_zone : "London"));
                    this._fillDropdownCollection(config.timeZones, config.timeZoneConvertedPrefix, this.timeZonesCollection);
                }
                
                this.$(".bb-timezone").empty();
                listView = new ConsumerViews.SalusDropdownWithValidation({
                    collection: this.timeZonesCollection,
                    viewModel: new App.Consumer.Models.DropdownViewModel({
                        id: "gatewayTimeZoneSelect",
                        dropdownLabelKey: "setupWizard.newUserForm.formFields.timeZoneSelect",
                        labelTextKey: "setupWizard.newUserForm.formFields.timeZoneSelect"
                    }),
                    required: true,
                    boundAttribute: "time_zone",
                    model: this.model
                });
                
                this.$(".bb-timezone").append(listView.render().$el);
            },
            
            // Bug_CASBW-28
            /**
            setTimeZone: function () {
                var address, timeZone;
                address = this.model.get("address");
                if(address && address.get("country") === "us") {
                    timeZone = config.timeZonePrefix_us + "." + this.model.get("time_zone");
                } else {
                    timeZone = config.timeZonePrefix + "." + this.model.get("time_zone");
                }                
                this.model.set("tzValue", timeZone);
            },
            **/
            
            onTimeZoneChange: function() {
                var arr = this.timeZone ? this.timeZone.split("/") : null;
                var time_zone = arr ? arr[arr.length-1] : this.timeZone;
                if(time_zone === this.model.get("time_zone")) {
                    return ;
                }
                this.saveButton.currentView.enable();
                var address, timeZone;
                address = this.model.get("address");
                // Bug_CASBW-28	Start
                if(address && address.get("country") === "us") {
                    this.model.set("currentTimeZone_us", this.model.get("time_zone"));
                } else {
                    this.model.set("currentTimeZone", this.model.get("time_zone"));
                }
                // Bug_CASBW-28 End
            },

			handleCancelClick: function () {
				this.model.restoreState();

				if (this.mode === "create") {
					App.navigate("dashboard");
					return;
				}

				_.each(this.registeredRegions, function (view) {
					if (view.resetView) {
						view.resetView();
					}
				});

				this.trigger("edit");
				this.trigger("clicked:cancel");
			},

			onRender: function () {
				this.collapseOrExpand(!this.startExpanded);

				this.$el.addClass("mode-" + this.options.mode);

				if (App.getIsEU()) {
					this.ui.gatewayStateWrapper.addClass("hidden");
				}

				if (!this.model.get("dsn")) {
					this.ui.addressContainer.hide();
				}
                
                if(this.mode === "edit"){
                    this.ui.gatewayTipsHeader.hide();
                }else{
                    this.ui.gatewayTipsHeader.show();
                }

				this.model.saveState();
			},

			onShow: function () {
				if (this.mode === "edit") {
					if (this.refreshButton.currentView) {
						this.refreshButton.$el.hide();
					}
				}
			},

			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});               
			},

			validateForm: function () {
				if (this.isValid()) {
					this.saveButton.currentView.enable();
				} else {
					this.saveButton.currentView.disable();
				}
			},

			handleSubmitClicked: function () {
				var that = this,
					gatewayDevice = this.model,
					shouldShowErrors = true;
                    
				// make sure the form validates before we submit
				if (!this.isValid(shouldShowErrors)) {
					return;
				}
                // Bug_CASBW-28
                // this.setTimeZone();
				if (this.mode === "create" || this.mode === "add") {
					this.saveButtonView.showSpinner();
                    // Bug_CASBW-28
					gatewayDevice.registerGateway().then(that.model.updateTimeZone).then(function () {
                        //set gateway default to pin
                        App.salusConnector.pinToDashboard(gatewayDevice, "device");
						that.trigger("edit");
						that.trigger("added:device");
						that.saveButtonView.hideSpinner();
                        // Bug_CASBW-28
                        that.saveButton.currentView.disable();
					}).catch(function (result) {
						that.saveButtonView.hideSpinner();
						if (result === "GatewayRegistrationTimedOut") {
                            that.saveButtonView.disable();
                            gatewayDevice.set("dsn", "");
							gatewayDevice.setFieldError("dsn", "equipment.gateway.error.registrationTimeout");
						} else if (result === "GatewayNodesNotFound") {	
                            //make "Active gateway" to grey
                            that.saveButtonView.disable();
                            gatewayDevice.set("dsn", "");
                            gatewayDevice.setFieldError("dsn", "equipment.gateway.error.registrationTimeout");
						} else {
							App.error(result);
                            gatewayDevice.setFieldError("dsn", "equipment.gateway.error.registrationTimeout");
						}
					});
				} else {
                    if(!this.saveButton.currentView.isEnabled()){
						return;
					}
					this.model.set("product_name", this.deviceName.currentView.getValue());
					this.model.updateTimeZone()
                        .then(this.model.saveProductName())
                        .then(this.model.persist())
						.then(this._updateAddress)
						.then(function () {
							that.model.saveState();
							that.trigger("edit");
                            // Bug_CASBW-28
                            that.saveButton.currentView.disable();
						}).catch(function () {
							//Stay on edit screen.
						});
				}
			},

			_updateAddress: function () {
				var address = this.model.get("address");

				return address.persist();
			},
            
            _onGatewayAddSuccess: function(){
                var gatewayDevice = this.model;
                this.saveButtonView.enable();
                gatewayDevice.clearErrors("dsn");          
                this.refreshButtonView.hideSpinner();
            },
            
            _onGatewayAddFailed: function(){
                var gatewayDevice = this.model;
                this.refreshButtonView.hideSpinner();
                gatewayDevice.set("dsn", "");
                gatewayDevice.setFieldError("dsn", "equipment.gateway.error.invalidDeviceSerialNumber");
            },
            
            searchRefresh: function(){
                var that = this;
                this.refreshInterval = setInterval(function(){
                    var gatewayDevice = that.model;
                    gatewayDevice.clearErrors("dsn");
                    gatewayDevice.openGatewayRegistration().then(function (data) {
                        if (data && data.device && data.device.dsn) {
                            gatewayDevice.set("dsn", data.device.dsn);

                            that._onGatewayAddSuccess();
                            clearInterval(that.refreshInterval);
                            clearTimeout(that.refreshPoll);
                        }
                     });
                }, 2 * 1000);
                this.refreshPoll = setTimeout(function(){
                    that._onGatewayAddFailed();
                    clearInterval(that.refreshInterval);
                    clearTimeout(that.refreshPoll);
                }, 2 * 60 * 1000);
                
            },
            
           
			handleRefreshClick: function () {
                var gatewayDevice = this.model, that = this;
				gatewayDevice.clearErrors("dsn");
                that.refreshButtonView.showSpinner();
                that.model.set("dsn", null);
                gatewayDevice.openGatewayRegistration().then(function (data) {
					if (data && data.device && data.device.dsn) {
						gatewayDevice.set("dsn", data.device.dsn);
                        that._onGatewayAddSuccess();
					} else {
                      //  gatewayDevice.setFieldError("dsn", "equipment.gateway.error.invalidDeviceSerialNumber");
                        that._onGatewayAddFailed();
					}
                 }).catch(function (result) {
                    if (result && result.status === 404) {
                        that.searchRefresh();
                    }
                });
			},
            
            onDestroy: function() {
                this.stopListening();
            }

		}).mixin([ConsumerMixins.SalusCollapsableViewMixin, ConsumerMixins.RegisteredRegions, ConsumerMixins.FormWithValidation]);

	});

	return App.Consumer.Equipment.Views.GatewayForm;
});
