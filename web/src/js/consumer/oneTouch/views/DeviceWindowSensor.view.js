"use strict";


define([
	"app",
	"bluebird",
    "consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
    "common/constants",
    "common/model/salusWebServices/rules/recommendedRules",
    "common/model/salusWebServices/rules/RuleDetails.model",
],function (App, P, consumerTemplates, SalusPageMixin, constants, ruleMap, Models){
    App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
        Views.DeviceWindowSensor = Mn.LayoutView.extend({
            className: "device-window-sensor-page container",
            template: consumerTemplates["oneTouch/deviceWindowSensor"],
            regions: {
                "checkboxRegion": ".bb-checkbox",
                "equipmentListRegion": ".bb-equipment-available",
                "saveButtonRegion": ".bb-save-button",
                "cancelButtonRegion": ".bb-cancel-button"
            },
            initialize: function (options) {
                var that=this,isChecked=false;
                
                _.bindAll(this, "handleFinishClick", "handleCancelClick");
                
                this.device=App.salusConnector.getDeviceByKey(options.key);
                this.rules=[];
                
                var ruleCollection = App.salusConnector.getRuleCollection();

                if (ruleCollection) {
                    ruleCollection.refresh().then(function (){
                        if (ruleCollection.models.length) {
                            that.rules=ruleCollection.filter(function (rule){
                                return rule.get("name").indexOf(constants.hideOneTouchKey + "-" + that.device.getEUID())===0;
                            });

                            // and rule
                            that.andRule=_.find(that.rules,function (rule){
                                if(rule.get("name").indexOf("-AND")!==-1){
                                    return rule;
                                }
                            });
                            
                            // or rule
                            that.orRule=_.find(that.rules,function (rule){
                                if(rule.get("name").indexOf("-OR")!==-1){
                                    return rule;
                                }
                            });
                            
                            //判断lock跟unlock是否钩选
                            _.each(that.rules,function (rule){
                                if(rule.get("name").indexOf("-AND")!==-1 && rule.get("actions").length===2){
                                    isChecked=true;
                                }

                            });
                        }
                        
                        that.checkboxModel=new App.Consumer.Models.CheckboxViewModel({
                            name: "equipmentCheckbox",
//                            nonKeyLabel: App.translate("equipment.thermostat.menus.modeLabels.lock"),
                            secondaryIconClass: "",
                            isChecked: isChecked
                        });


                        that.checkbox=new App.Consumer.Views.CheckboxView({
                            model: that.checkboxModel
                        });

                    });
                }
                
            },
            onRender: function (){
                var that=this;

                
                var showDevice=App.salusConnector.getWindowSensorCollection();
                showDevice.models=showDevice.filter(function (device){
                    if(device.get("LeaveNetwork") && device.get("OnlineStatus_i") && (device.get("connection_status")!=="Offline")){
                        return device;
                    }
                });
                        
                
                App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
                    that.equipmentListRegion.show(new App.Consumer.Equipment.Views.EquipmentCollectionView({
						collection: showDevice,
                        deviceType: "all",
                        windowSensorRule: that.andRule
//                        offlineLabel: true
					}));
                });
                
                this.checkboxRegion.show(this.checkbox);
                
                this.saveButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.save",
					clickedDelegate: that.handleFinishClick
				}));
                
                this.cancelButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: that.handleCancelClick
				}));
            },
            
            handleFinishClick: function(event, button){
                var that=this,windowSensors=[],keys=this.equipmentListRegion.currentView.getCheckedItemKeys(),
                         openRuleModel,closeRuleModel;

                
                if(keys.length){
                    if(this.rules && this.rules.length){
                        // update
                        
                        //1.找出新增加的
                        var euids=[];
                        _.each(keys,function(key){
                            var device=App.salusConnector.getDeviceByKey(key)
                            euids.push(device.getEUID());
                            if(!that.rules[0].get("conditions").findWhere({EUID:device.getEUID()})){
                                var omitCondition0=new Models.RuleCondition;
                                omitCondition0.set(that.rules[0].get("conditions").models[0].toJSON());
                                omitCondition0.set("EUID",device.getEUID());
                                that.rules[0].get("conditions").add(omitCondition0);
                                
                                
                                var omitCondition1=new Models.RuleCondition;
                                omitCondition1.set(that.rules[1].get("conditions").models[0].toJSON());
                                omitCondition1.set("EUID",device.getEUID());
                                that.rules[1].get("conditions").add(omitCondition1);
                                
                            }
                        });
                        
                        //2.找出被删除的
                        this.rules[0].get("conditions").reset(this.rules[0].get("conditions").filter(function (condition){
                           if($.inArray(condition.get("EUID"),euids)!==-1){
                               return condition;
                           }
                        }));
                        this.rules[1].get("conditions").reset(this.rules[1].get("conditions").filter(function (condition){
                           if($.inArray(condition.get("EUID"),euids)!==-1){
                               return condition;
                           }
                        }));
                        
                        
                        //3.更新lock跟unlock
                        var orRuleActions=this.orRule.get("actions").models;
                        var andRuleActions=this.andRule.get("actions").models;
                        if(this.checkbox.getIsChecked()){
                            if(orRuleActions.length!==2){
                                var newAction=new Models.RuleAction;
                                newAction.set(ruleMap[constants.modelTypes.WINDOWMONITOR][0].actions[1]);
                                newAction.set("EUID",orRuleActions[0].get("EUID"));
                                newAction.set("propName",this.device.getSetterPropNameIfPossible(newAction.get("propName")));
                                this.orRule.get("actions").add(newAction);
                            }
                            if(andRuleActions.length!==2){
                                var newAction=new Models.RuleAction;
                                newAction.set(ruleMap[constants.modelTypes.WINDOWMONITOR][1].actions[1]);
                                newAction.set("EUID",andRuleActions[0].get("EUID"));
                                newAction.set("propName",this.device.getSetterPropNameIfPossible(newAction.get("propName")));
                                this.andRule.get("actions").add(newAction);
                            }
                        } else {
                            if(orRuleActions.length===2){
                                this.orRule.get("actions").remove(orRuleActions[1]);
                            }
                            if(andRuleActions.length===2){
                                this.andRule.get("actions").remove(andRuleActions[1]);
                            }
                        }
                        
                        this.saveButtonRegion.currentView.showSpinner();
                        P.all([this.rules[0].update(),this.rules[1].update()]).then(function (){
                            window.history.back();
                        });
                        
                    }else{
                        // add
                        _.each(keys,function (key){
                            windowSensors.push(App.salusConnector.getDeviceByKey(key)); 
                        });

                        openRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});
                        closeRuleModel=new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});


                        openRuleModel.buildWindowOpenRule(windowSensors, this.device, this.checkbox.getIsChecked());
                        closeRuleModel.buildWindowCloseRule(windowSensors, this.device, this.checkbox.getIsChecked());

                        this.saveButtonRegion.currentView.showSpinner();
                        P.all([openRuleModel.add(),closeRuleModel.add()]).then(function (){
                            window.history.back();
                        });
                    }
                } else if(this.rules && this.rules.length){
                    // delete
                    this.saveButtonRegion.currentView.showSpinner();
                    P.all([this.rules[0].unregister(),this.rules[1].unregister()]).then(function (){
                        window.history.back();
                    });
                    
                }
                
                
            },
            handleCancelClick: function (){
                window.history.back();
            }
        }).mixin([SalusPageMixin]);
    });
    
    return App.Consumer.OneTouch.Views.DeviceWindowSensor;
});

