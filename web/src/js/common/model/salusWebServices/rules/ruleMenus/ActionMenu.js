"use strict";

define([
	"app",
	"common/constants",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, constants) {
	App.module("Models", function (Models, App, B) {
		Models.ActionMenu = B.Model.extend({
			defaults: {
				menu: null,
				equipmentArray: null
			},
			initialize: function (equipmentArray) {
				this.equipmentArray = equipmentArray;
				this._buildMenu();
			},
			_buildMenu: function (){
				this.set("menu", {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection([
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.doThis.root.notifyMe"
						}, {childMenu: {
                                menuModel: new B.Model({
                                    uniqueView: "contact-info",
                                    type: constants.oneTouchMenuTypes.action,
                                    isLeafMenu: true
							})
                        }}),
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.doThis.root.propertyChange"
						}, {childMenu: this._changeProperty()})
					])
				});
			},
			_changeProperty: function () {
				return {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.action,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection(this.equipmentArray.getEquipmentMenuForRule(constants.oneTouchMenuTypes.action))
				};
			}
		});
	});

	return App.Models.ActionMenu;
});